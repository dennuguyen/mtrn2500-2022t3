#include <iostream>
#include <webots/Robot.hpp>

constexpr int duration{256};  // Robot cycle duration.

int main() {
    webots::Robot controller;  // Models a robot's controller e.g. an Arduino Uno.
    int i{0};
    while (controller.step(duration) != -1) {
        std::cout << i++ << std::endl;
    }
}