#pragma once

#include <webots/Motor.hpp>

constexpr double maxEpuckMotorSpeed{6.28};

void halt(webots::Motor& leftMotor, webots::Motor& rightMotor);
void moveForward(webots::Motor& leftMotor, webots::Motor& rightMotor);
void moveBackward(webots::Motor& leftMotor, webots::Motor& rightMotor);
void turnLeft(webots::Motor& leftMotor, webots::Motor& rightMotor);
void turnRight(webots::Motor& leftMotor, webots::Motor& rightMotor);
