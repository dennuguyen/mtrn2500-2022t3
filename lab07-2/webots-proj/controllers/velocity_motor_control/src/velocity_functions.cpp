#include "velocity_functions.hpp"

void halt(webots::Motor& leftMotor, webots::Motor& rightMotor) {
    leftMotor.setVelocity(0);
    rightMotor.setVelocity(0);
}

void moveForward(webots::Motor& leftMotor, webots::Motor& rightMotor) {
    leftMotor.setPosition(INFINITY);
    rightMotor.setPosition(INFINITY);
    leftMotor.setVelocity(maxEpuckMotorSpeed);
    rightMotor.setVelocity(maxEpuckMotorSpeed);
}

void moveBackward(webots::Motor& leftMotor, webots::Motor& rightMotor) {
    leftMotor.setPosition(INFINITY);
    rightMotor.setPosition(INFINITY);
    leftMotor.setVelocity(-maxEpuckMotorSpeed);
    rightMotor.setVelocity(-maxEpuckMotorSpeed);
}

void turnLeft(webots::Motor& leftMotor, webots::Motor& rightMotor) {
    leftMotor.setPosition(INFINITY);
    rightMotor.setPosition(INFINITY);
    leftMotor.setVelocity(-maxEpuckMotorSpeed / 2);
    rightMotor.setVelocity(maxEpuckMotorSpeed / 2);
}

void turnRight(webots::Motor& leftMotor, webots::Motor& rightMotor) {
    leftMotor.setPosition(INFINITY);
    rightMotor.setPosition(INFINITY);
    leftMotor.setVelocity(maxEpuckMotorSpeed / 2);
    rightMotor.setVelocity(-maxEpuckMotorSpeed / 2);
}