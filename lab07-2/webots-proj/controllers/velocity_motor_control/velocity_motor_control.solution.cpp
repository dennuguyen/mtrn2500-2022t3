#include <webots/Motor.hpp>
#include <webots/Robot.hpp>

constexpr int duration{64};  // Robot cycle duration.
constexpr double maxEpuckMotorSpeed{6.28};

void wait(webots::Robot& robot, double waitPeriod) {
    const double start{robot.getTime()};
    while (robot.getTime() - start < waitPeriod * 0.001) {
        robot.step(duration);
    }
}

void halt(webots::Motor& leftMotor, webots::Motor& rightMotor) {
    leftMotor.setVelocity(0);
    rightMotor.setVelocity(0);
}

void moveForward(webots::Motor& leftMotor, webots::Motor& rightMotor) {
    // Set the angular positions to INFINITY so we can infinitely spin the motor.
    leftMotor.setPosition(INFINITY);
    rightMotor.setPosition(INFINITY);

    // Set the angular velocities.
    leftMotor.setVelocity(maxEpuckMotorSpeed);
    rightMotor.setVelocity(maxEpuckMotorSpeed);
}

void moveBackward(webots::Motor& leftMotor, webots::Motor& rightMotor) {
    leftMotor.setPosition(INFINITY);
    rightMotor.setPosition(INFINITY);
    leftMotor.setVelocity(-maxEpuckMotorSpeed / 2);
    rightMotor.setVelocity(-maxEpuckMotorSpeed / 2);
}

void turnLeft(webots::Motor& leftMotor, webots::Motor& rightMotor) {
    leftMotor.setPosition(INFINITY);
    rightMotor.setPosition(INFINITY);
    leftMotor.setVelocity(-maxEpuckMotorSpeed / 2);  // Negative.
    rightMotor.setVelocity(maxEpuckMotorSpeed / 2);  // Positive.
}

int main() {
    webots::Robot robot;
    webots::Motor& leftMotor{*robot.getMotor("left wheel motor")};
    webots::Motor& rightMotor{*robot.getMotor("right wheel motor")};

    // Set initial velocity.
    halt(leftMotor, rightMotor);

    {  // Completing just moveForward.
        moveForward(leftMotor, rightMotor);
    }

    {  // Blocking timer demonstration.
        while (robot.step(duration) != -1) {
            moveForward(leftMotor, rightMotor);
            wait(robot, 1000);
            halt(leftMotor, rightMotor);
            wait(robot, 1000);
        }
    }

    {  // Completing the state flow of velocity_move_control.
        while (robot.step(duration) != -1) {
            moveForward(leftMotor, rightMotor);
            wait(robot, 1000);
            moveBackward(leftMotor, rightMotor);
            wait(robot, 2000);
            turnLeft(leftMotor, rightMotor);
            wait(robot, 1000);
        }
    }
}
