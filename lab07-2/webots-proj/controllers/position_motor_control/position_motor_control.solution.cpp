// Replace Webot's provided main file with this because the former contains a lot of bad style.

#include <iostream>

// Remember to #include these files.
#include <webots/Motor.hpp>
#include <webots/PositionSensor.hpp>
#include <webots/Robot.hpp>

constexpr int duration{64};  // Robot cycle duration.
constexpr double maxEpuckMotorSpeed{6.28};
constexpr double epuckWheelRadius{0.0206};
constexpr double epuckAxleLength{0.052};
constexpr double moveForwardRotation{0.5 * 0.5 / epuckWheelRadius};
constexpr double turn90DegRotation{(epuckAxleLength * 0.5) * (M_PI * 0.5) / epuckWheelRadius};

void wait(webots::Robot& robot, double waitPeriod) {
    const double start{robot.getTime()};
    while (robot.getTime() - start < waitPeriod * 0.001) {
        robot.step(duration);
    }
}

void halt(webots::Motor& leftMotor, webots::Motor& rightMotor) {
    leftMotor.setVelocity(0);
    rightMotor.setVelocity(0);
}

// Without position sensor.
void moveForward(webots::Motor& leftMotor, webots::Motor& rightMotor) {
    leftMotor.setPosition(moveForwardRotation);
    rightMotor.setPosition(moveForwardRotation);
    leftMotor.setVelocity(maxEpuckMotorSpeed);
    rightMotor.setVelocity(maxEpuckMotorSpeed);
}

// With position sensor.
void moveForward(webots::Motor& leftMotor, webots::Motor& rightMotor,
                 webots::PositionSensor& leftSensor, webots::PositionSensor& rightSensor) {
    // Get the current position of the motors.
    auto leftInitial = leftSensor.getValue();
    auto rightInitial = rightSensor.getValue();

    // Set the motor positions.
    leftMotor.setPosition(leftInitial + moveForwardRotation);    // Addition.
    rightMotor.setPosition(rightInitial + moveForwardRotation);  // Addition.

    leftMotor.setVelocity(maxEpuckMotorSpeed);
    rightMotor.setVelocity(maxEpuckMotorSpeed);
}

void moveBackward(webots::Motor& leftMotor, webots::Motor& rightMotor,
                  webots::PositionSensor& leftSensor, webots::PositionSensor& rightSensor) {
    auto leftInitial = leftSensor.getValue();
    auto rightInitial = rightSensor.getValue();

    leftMotor.setPosition(leftInitial - moveForwardRotation);    // Subtraction.
    rightMotor.setPosition(rightInitial - moveForwardRotation);  // Subtraction.

    leftMotor.setVelocity(maxEpuckMotorSpeed);
    rightMotor.setVelocity(maxEpuckMotorSpeed);
}

void turnRight(webots::Motor& leftMotor, webots::Motor& rightMotor,
               webots::PositionSensor& leftSensor, webots::PositionSensor& rightSensor) {
    auto leftInitial = leftSensor.getValue();
    auto rightInitial = rightSensor.getValue();

    leftMotor.setPosition(leftInitial + turn90DegRotation);    // Addition.
    rightMotor.setPosition(rightInitial - turn90DegRotation);  // Subtraction.

    leftMotor.setVelocity(maxEpuckMotorSpeed / 2);  // Slow down for more accuracy.
    rightMotor.setVelocity(maxEpuckMotorSpeed / 2);
}

int main() {
    webots::Robot robot;
    webots::Motor& leftMotor{*robot.getMotor("left wheel motor")};
    webots::Motor& rightMotor{*robot.getMotor("right wheel motor")};

    halt(leftMotor, rightMotor);

    {  // Just moving forward 0.5 m.
        moveForward(leftMotor, rightMotor);
    }

    {  // Moving forward 0.5 m twice.
        moveForward(leftMotor, rightMotor);
        moveForward(leftMotor, rightMotor);
    }

    {  // Moving forward 0.5 m twice with wait.
        moveForward(leftMotor, rightMotor);
        wait(robot, 3000);  // Wait for previous command to finish.
        moveForward(leftMotor, rightMotor);
    }

    webots::PositionSensor& leftSensor{*robot.getPositionSensor("left wheel sensor")};
    webots::PositionSensor& rightSensor{*robot.getPositionSensor("right wheel sensor")};
    leftSensor.enable(duration);
    rightSensor.enable(duration);

    {  // State flow for position_motor_control.
        while (robot.step(duration) != -1) {
            moveForward(leftMotor, rightMotor, leftSensor, rightSensor);
            wait(robot, 4000);
            moveBackward(leftMotor, rightMotor, leftSensor, rightSensor);
            wait(robot, 4000);
            turnRight(leftMotor, rightMotor, leftSensor, rightSensor);
            wait(robot, 2000);
        }
    }
}
