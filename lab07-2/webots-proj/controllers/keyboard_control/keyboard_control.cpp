#include <iostream>
#include <webots/Keyboard.hpp>
#include <webots/Motor.hpp>
#include <webots/Robot.hpp>

#include "velocity_functions.hpp"

constexpr int duration{64};  // Robot cycle duration.

int main() {
    webots::Robot robot;

    webots::Motor& leftMotor{*robot.getMotor("left wheel motor")};
    webots::Motor& rightMotor{*robot.getMotor("right wheel motor")};
    halt(leftMotor, rightMotor);

    webots::Keyboard& keyboard{*robot.getKeyboard()};
    keyboard.enable(duration);

    while (robot.step(duration) != -1) {
        char const key = static_cast<char>(keyboard.getKey());
        std::cout << key << std::endl;
        switch (key) {
            case 'W':
                moveForward(leftMotor, rightMotor);
                break;
            case 'A':
                turnLeft(leftMotor, rightMotor);
                break;
            case 'S':
                moveBackward(leftMotor, rightMotor);
                break;
            case 'D':
                turnRight(leftMotor, rightMotor);
                break;
            case ' ':
                halt(leftMotor, rightMotor);
                break;
        }
    }
}
