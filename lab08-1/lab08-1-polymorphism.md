# MTRN2500 - Lab08-1 (Polymorphism)

## Learning Outcomes

- Concept of polymorphism.
- Can use dynamic cast to typecast pointers/references to objects up the inheritance hierarchy.
- Understand the purpose of virtual functions.
- Understand what pure virtual functions are and how they enforce abstract classes.
- Understand the purpose of virtual destructors.

---

## Exercise: Encapsulating Controllers

Create a controller, `base_robot`, with a class, `BaseRobot`, which encapsulates the functions and Webots devices from `keyboard_control` and `velocity_motor_control` from `lab07-2`.

`BaseRobot` should compose:
- A robot controller of type `webots::Robot`.
- The left wheel motor of type `webots::Motor&`.
- The right wheel motor of type `webots::Motor&`.
- A keyboard of type `webots::Keyboard&`.

The public interface for `BaseRobot` should have the following methods:

<table>
    <tr>
        <th>Method</th>
        <th>Description</th>
        <th>Usage</th>
        <th>Exceptions</th>
    </tr>
    <tr>
        <td><code>BaseRobot(int)</code></td>
        <td>Creates a robot controller with a specified cycle duration.</td>
        <td><pre><code>BaseRobot r(64);</code></pre></td>
        <td>None.</td>
    </tr>
    <tr>
        <td><code>run()</code></td>
        <td>Executes the robot's control loop. The default robot behaviour of the control loop is to accept teleoperation commands to move the robot.</td>
        <td><pre><code>BaseRobot r(64);
r.run();</code></pre></td>
        <td>None.</td>
    </tr>
    <tr>
        <td><code>void halt()</code></td>
        <td>Stops robot movement.</td>
        <td><pre><code>BaseRobot r(64);
r.halt();</code></pre></td>
        <td>None.</td>
    </tr>
    <tr>
        <td><code>void moveForward()</code></td>
        <td>Moves the robot forward at the robot's maximum speed.</td>
        <td><pre><code>BaseRobot r(64);
r.moveForward();</code></pre></td>
        <td>None.</td>
    </tr>
    <tr>
        <td><code>void moveBackward()</code></td>
        <td>Moves the robot backward at the robot's maximum speed.</td>
        <td><pre><code>BaseRobot r(64);
r.moveBackward();</code></pre></td>
        <td>None.</td>
    </tr>
    <tr>
        <td><code>void turnLeft()</code></td>
        <td>Turns the robot left on the spot at half the robot's maximum speed.</td>
        <td><pre><code>BaseRobot r(64);
r.turnLeft();</code></pre></td>
        <td>None.</td>
    </tr>
    <tr>
        <td><code>void turnRight()</code></td>
        <td>Turns the robot right on the spot at half the robot's maximum speed.</td>
        <td><pre><code>BaseRobot r(64);
r.turnRight();</code></pre></td>
        <td>None.</td>
    </tr>
</table>



Create a controller, `autonomous_robot`, with a class, `AutonomousRobot`, which inherits `BaseRobot`.

The public interface for `BaseRobot` should only have the following methods:

<table>
    <tr>
        <th>Method</th>
        <th>Description</th>
        <th>Usage</th>
        <th>Exceptions</th>
    </tr>
    <tr>
        <td><code>AutonomousRobot(int)</code></td>
        <td>Creates a robot controller with a specified cycle duration.</td>
        <td><pre><code>AutonomousRobot r(64);</code></pre></td>
        <td>None.</td>
    </tr>
    <tr>
        <td><code>run()</code></td>
        <td>Executes the robot's control loop. The robot behaviour is to move forward for <code>1 s</code>, move backward for <code>1 s</code>, turn left for <code>0.5 s</code>, and repeat.</td>
        <td><pre><code>AutonomousRobot r(64);
r.run();</code></pre></td>
        <td>None.</td>
    </tr>
</table>



---

## Concept of Polymorphism

Define polymorphism.



What are some real-life examples of polymorphism?



What is an example of polymorphism that has already been covered in this course?



---

## Exercise: Dynamic Casting

How does `dynamic_cast` differ from `static_cast`?



Consider the following classes.

```cpp
struct Base {
    void foo() { std::cout << "Base." << std::endl; }
};

struct Derived : public Base {
    void foo() { std::cout << "Derived." << std::endl; }
};
```

Demonstrate up-casting (i.e. from `Derived*` to `Base*`). What is the expected output of `foo()` on the casted object?



Demonstrate up-casting with `BaseRobot` and `AutonomousRobot`. What is the expected control loop behaviour?



---

## Exercise: Virtual Functions

What are virtual functions?



What is the syntax for marking and overriding a virtual function?



Modify `Derived` and `Base` such that `foo()` is appropriately marked `virtual` and `override`. What is the expected output of `foo()` on the casted object?



Modify `AutonomousRobot` and `BaseRobot` such that `run()` is appropriately marked `virtual` and `override`. What is the expected control loop behaviour on the up-casted object?



---

## Exercise: Pure Virtual Functions

What is the syntax for declaring a pure virtual function?



How are virtual functions different from pure virtual functions?



What is an abstract class?



Consider the classes `Base` and `Derived` from [Exercise: Virtual Functions](#exercise-virtual-functions). Make `Base` an abstract class and demonstrate that:
- `Base` cannot be instantiated.
- `Derived` cannot be instantiated without implementing the pure virtual function.



Modify `BaseRobot` such that it is an abstract class. What is an appropriate function to make pure virtual?



---

## Exercise: Virtual Destructor

Consider the following program which shows the order of construction and destruction via an up-casted pointer:
```cpp
#include <iostream>

struct Base {
    Base() { std::cout << "Creating Base." << std::endl; }
    ~Base() { std::cout << "Destroying Base." << std::endl; }
};

struct Derived : public Base {
    Derived() { std::cout << "Creating Derived." << std::endl; }
    ~Derived() { std::cout << "Destroying Derived." << std::endl; }
};

int main() {
    Base* b = new Derived;  // Up-cast.
    delete b;
}
```

What is the output of the program?



Modify the program so that `Base::~Base()` is marked `virtual`. What is the output of the program?



---

## Feedback

If you liked or disliked anything about the labs, please leave some [feedback](https://forms.office.com/r/sV4X0xR7dT)!
