# MTRN2500 - Lab08-1 (Polymorphism)

## Learning Outcomes

- Concept of polymorphism.
- Can use dynamic cast to typecast pointers/references to objects up the inheritance hierarchy.
- Understand the purpose of virtual functions.
- Understand what pure virtual functions are and how they enforce abstract classes.
- Understand the purpose of virtual destructors.

---

## Exercise: Encapsulating Controllers

Create a controller, `base_robot`, with a class, `BaseRobot`, which encapsulates the functions and Webots devices from `keyboard_control` and `velocity_motor_control` from `lab07-2`.

`BaseRobot` should compose:
- A robot controller of type `webots::Robot`.
- The left wheel motor of type `webots::Motor&`.
- The right wheel motor of type `webots::Motor&`.
- A keyboard of type `webots::Keyboard&`.

The public interface for `BaseRobot` should have the following methods:

<table>
    <tr>
        <th>Method</th>
        <th>Description</th>
        <th>Usage</th>
        <th>Exceptions</th>
    </tr>
    <tr>
        <td><code>BaseRobot(int)</code></td>
        <td>Creates a robot controller with a specified cycle duration.</td>
        <td><pre><code>BaseRobot r(64);</code></pre></td>
        <td>None.</td>
    </tr>
    <tr>
        <td><code>run()</code></td>
        <td>Executes the robot's control loop. The default robot behaviour of the control loop is to accept teleoperation commands to move the robot.</td>
        <td><pre><code>BaseRobot r(64);
r.run();</code></pre></td>
        <td>None.</td>
    </tr>
    <tr>
        <td><code>void halt()</code></td>
        <td>Stops robot movement.</td>
        <td><pre><code>BaseRobot r(64);
r.halt();</code></pre></td>
        <td>None.</td>
    </tr>
    <tr>
        <td><code>void moveForward()</code></td>
        <td>Moves the robot forward at the robot's maximum speed.</td>
        <td><pre><code>BaseRobot r(64);
r.moveForward();</code></pre></td>
        <td>None.</td>
    </tr>
    <tr>
        <td><code>void moveBackward()</code></td>
        <td>Moves the robot backward at the robot's maximum speed.</td>
        <td><pre><code>BaseRobot r(64);
r.moveBackward();</code></pre></td>
        <td>None.</td>
    </tr>
    <tr>
        <td><code>void turnLeft()</code></td>
        <td>Turns the robot left on the spot at half the robot's maximum speed.</td>
        <td><pre><code>BaseRobot r(64);
r.turnLeft();</code></pre></td>
        <td>None.</td>
    </tr>
    <tr>
        <td><code>void turnRight()</code></td>
        <td>Turns the robot right on the spot at half the robot's maximum speed.</td>
        <td><pre><code>BaseRobot r(64);
r.turnRight();</code></pre></td>
        <td>None.</td>
    </tr>
</table>

<details><summary>Answer</summary><p>

The `BaseRobot` class definition may look like:
- All the members are marked as `protected` because `AutonomousRobot` requires them.
- `wait()` is a protected helper function.
```cpp
#pragma once

#include "webots/Keyboard.hpp"
#include "webots/Motor.hpp"
#include "webots/Robot.hpp"

class BaseRobot {
public:
    BaseRobot(int);
    void run();
    void halt();
    void moveForward();
    void moveBackward();
    void turnLeft();
    void turnRight();

protected:
    void wait(double);

    static constexpr double mMaxMotorSpeed{6.28};
    int const mDuration;

    webots::Robot mController;
    webots::Motor& mLeftMotor;
    webots::Motor& mRightMotor;
    webots::Keyboard& mKeyboard;
};
```

The `BaseRobot` constructor definition may look like:
- An initialiser list is used to initialise `const` and `reference` members.
- The constructor function can be used to set an initial state for `BaseRobot`:
    - The keyboard is enabled.
    - `halt()` is called.
```cpp
BaseRobot::BaseRobot(int duration)
    : mDuration(duration),
      mController(webots::Robot()),
      mLeftMotor(*mController.getMotor("left wheel motor")),
      mRightMotor(*mController.getMotor("right wheel motor")),
      mKeyboard(*mController.getKeyboard()) {
    mKeyboard.enable(mDuration);
    halt();
}
```

The control loop may look like:
```cpp
void BaseRobot::run() {
    while (mController.step(mDuration) != -1) {
        char const key = static_cast<char>(mKeyboard.getKey());
        std::cout << key << std::endl;
        switch (key) {
            case 'W':
                moveForward();
                break;
            case 'A':
                turnLeft();
                break;
            case 'S':
                moveBackward();
                break;
            case 'D':
                turnRight();
                break;
            case ' ':
                halt();
                break;
        }
    }
}
```

Every other method is implemented the same as the functions from `velocity_motor_control` from `lab07-2`.

The main file is simplified to only instantiate `BaseRobot` and call `BaseRobot::run()`.
```cpp
#include "BaseRobot.hpp"

int main(int argc, char **argv) {
    BaseRobot r(64);
    r.run();
}
```

---

</p></details>

Create a controller, `autonomous_robot`, with a class, `AutonomousRobot`, which inherits `BaseRobot`.

The public interface for `BaseRobot` should only have the following methods:

<table>
    <tr>
        <th>Method</th>
        <th>Description</th>
        <th>Usage</th>
        <th>Exceptions</th>
    </tr>
    <tr>
        <td><code>AutonomousRobot(int)</code></td>
        <td>Creates a robot controller with a specified cycle duration.</td>
        <td><pre><code>AutonomousRobot r(64);</code></pre></td>
        <td>None.</td>
    </tr>
    <tr>
        <td><code>run()</code></td>
        <td>Executes the robot's control loop. The robot behaviour is to move forward for <code>1 s</code>, move backward for <code>1 s</code>, turn left for <code>0.5 s</code>, and repeat.</td>
        <td><pre><code>AutonomousRobot r(64);
r.run();</code></pre></td>
        <td>None.</td>
    </tr>
</table>

<details><summary>Answer</summary><p>

The `AutonomousRobot` class definition may look like:
- `BaseRobot` is inherited.
```cpp
class AutonomousRobot : public BaseRobot {
public:
    AutonomousRobot(int);
    void run();
};
```

The `AutonomousRobot` constructor may look like:
```cpp
AutonomousRobot::AutonomousRobot(int duration) : BaseRobot(duration) {}
```

The control loop may look like:
```cpp
void AutonomousRobot::run() {
    while (mController.step(mDuration) != -1) {
        moveForward();
        wait(1000);
        moveBackward();
        wait(1000);
        turnLeft();
        wait(500);
    }
}
```

The main file is simplified to only instantiate `AutonomousRobot` and call `AutonomousRobot::run()`.
```cpp
#include "AutonomousRobot.hpp"

int main(int argc, char **argv) {
    AutonomousRobot r(64);
    r.run();
}

```

---

</p></details>

---

## Concept of Polymorphism

Define polymorphism.

<details><summary>Answer</summary><p>

Polymorphism is the ability for an entity to take on many forms.

---

</p></details>

What are some real-life examples of polymorphism?

<details><summary>Answer</summary><p>

Recall any real-life inheritance examples.
- A dog can either act as a dog or (more generally) an animal.
- A rose can either act as a rose or (more generally) a flower.

---

</p></details>

What is an example of polymorphism that has already been covered in this course?

<details><summary>Answer</summary><p>

Function overloads are an example of polymorphism. It is a single function that has many forms.

```cpp
void overload();
void overload(int);
void overload(double);
```

The polymorphic nature of function overloads is determined at compile-time.

---

</p></details>

---

## Exercise: Dynamic Casting

How does `dynamic_cast` differ from `static_cast`?

<details><summary>Answer</summary><p>

Dynamic casting enables class polymorphism.

Dynamic casting is used to explicitly and safely typecast a pointer or reference to an object, up or down the inheritance hierarchy.

Dynamic casting is safe:
- Returns a nullptr if casting a pointer to an object goes wrong.
- Throws an exception if casting a reference to an object goes wrong.

Static casting does not do any run-time checks.

---

</p></details>

Consider the following classes.

```cpp
struct Base {
    void foo() { std::cout << "Base." << std::endl; }
};

struct Derived : public Base {
    void foo() { std::cout << "Derived." << std::endl; }
};
```

Demonstrate up-casting (i.e. from `Derived*` to `Base*`). What is the expected output of `foo()` on the casted object?

<details><summary>Answer</summary><p>

> Do this for students.

```cpp
Base b;
Derived d;
b.foo();  // Outputs "Base."
d.foo();  // Outputs "Derived."

Base* b_ptr{dynamic_cast<Base*>(&d)};
b_ptr->foo();  // Outputs "Base."
```

Down-casting is possible but dangerous. We should avoid it if possible.
- It is bad style because it usually doesn't make sense.
- If we do down-cast, then always check that the casted pointer is not `nullptr`.
    ```cpp
    Base b;
    Derived* d_ptr{dynamic_cast<Derived*>(&b)}
    if (d_ptr != nullptr) {
        std::cout << "Down-cast succeeded!\n";
    }
    ```

---

</p></details>

Demonstrate up-casting with `BaseRobot` and `AutonomousRobot`. What is the expected control loop behaviour?

<details><summary>Answer</summary><p>

Up-casting is from `Derived*` to `Base*` so we must up-cast from `AutonomousRobot*` to `BaseRobot*`. We must modify `autonomous_robot.cpp`:
```cpp
#include "AutonomousRobot.hpp"
#include "BaseRobot.hpp"

int main(int argc, char** argv) {
    AutonomousRobot r(64);
    BaseRobot* b{dynamic_cast<BaseRobot*>(&r)};
    b->run();  // Calls BaseRobot::run().
}
```

---

</p></details>

---

## Exercise: Virtual Functions

What are virtual functions?

<details><summary>Answer</summary><p>

["Virtual functions are member functions whose behaviour can be overriden in derived classes."](https://en.cppreference.com/w/cpp/language/virtual). Simply, virtual functions allow you to keep the overriding behaviour when an object has been dynamically casted.

---

</p></details>

What is the syntax for marking and overriding a virtual function?

<details><summary>Answer</summary><p>

The `virtual` keyword needs to be added to the start of a function signature in the `Base` class:
```cpp
struct Base {
    virtual void foo() { std::cout << "Base::foo() called." << std::endl; }
};
```
`foo` can be implemented with a behaviour that is specific to `Base`.

If `Derived` wants to change the behaviour of `foo`, then the `override` keyword is added at the end of a function signature.
```cpp
struct Derived : public Base {
    void foo() override { std::cout << "Derived::foo() called." << std::endl; }
};
```

The `override` keyword can be omitted but is good style to use because:
- It explicitly tells us the function is overriden.
- Compiler will check if the function signature is correct.
    ```cpp
    virtual void foo();
    void foo() override;     // Okay.
    void foo(int) override;  // Error.
    ```

---

</p></details>

Modify `Derived` and `Base` such that `foo()` is appropriately marked `virtual` and `override`. What is the expected output of `foo()` on the casted object?

<details><summary>Answer</summary><p>

> Do this for students.

```cpp
#include <iostream>

struct Base {
    virtual void foo() { std::cout << "Base." << std::endl; }
};

struct Derived : public Base {
    // Acceptable.
    virtual void foo() override { std::cout << "Derived." << std::endl; }

    // This is also acceptable if we don't expect `Derived` to be inherited again.
    void foo() override { std::cout << "Derived." << std::endl; }
};

int main() {
    Base b;
    Derived d;
    b.foo();  // Outputs "Base."
    d.foo();  // Outputs "Derived."

    Base* b_ptr = dynamic_cast<Base*>(&d);
    b_ptr->foo();  // Outputs "Derived."
}
```

`b_ptr->foo()` now outputs `"Derived."` instead of `"Base."`. Remember that the original implementation has been overriden.

---

</p></details>

Modify `AutonomousRobot` and `BaseRobot` such that `run()` is appropriately marked `virtual` and `override`. What is the expected control loop behaviour on the up-casted object?

<details><summary>Answer</summary><p>

Modify `autonomous_robot.cpp` to perform an up-cast:
```cpp
#include "AutonomousRobot.hpp"
#include "BaseRobot.hpp"

int main(int argc, char** argv) {
    AutonomousRobot r(64);
    BaseRobot* b{dynamic_cast<BaseRobot*>(&r)};
    b->run();
}
```

Modify the function signature of `BaseRobot::run()` to be:
```cpp
virtual void run();
```

Modify the function signature of `AutonomousRobot::run()` to be:
```cpp
virtual void run() override;
```

Since `run()` has been overriden, the expected control loop behaviour is the autonomous version.

---

</p></details>

---

## Exercise: Pure Virtual Functions

What is the syntax for declaring a pure virtual function?

<details><summary>Answer</summary><p>

End a virtual function with `= 0` instead of an implementation:
```cpp
struct Base {
    virtual void foo() = 0;  // Pure virtual function.
};
```

---

</p></details>

How are virtual functions different from pure virtual functions?

<details><summary>Answer</summary><p>

Virtual functions can be optionally overridden. If a virtual function is not overridden, then the default implementation is used.

Pure virtual functions must be overridden. You will not be able to instantiate any class that has a non-overriden pure virtual function.

---

</p></details>

What is an abstract class?

<details><summary>Answer</summary><p>

Abstract classes are just classes that are not allowed to be instantiated but can be inherited. This behaviour may be useful if you want to design an interface for a class.

Any class with a non-overridden pure virtual function is an abstract class. You must override the pure virtual function to be able to instantiate the class.

---

</p></details>

Consider the classes `Base` and `Derived` from [Exercise: Virtual Functions](#exercise-virtual-functions). Make `Base` an abstract class and demonstrate that:
- `Base` cannot be instantiated.
- `Derived` cannot be instantiated without implementing the pure virtual function.

<details><summary>Answer</summary><p>

> Do for students.

To demonstrate `Base` cannot be instantiated:
- Make `foo()` pure virtual.
- Attempt to compile the program.
```cpp
#include <iostream>

struct Base {
    virtual void foo() = 0;
};

struct Derived : public Base {
    void foo() override { std::cout << "Derived." << std::endl; }
};

int main() {
    Base b;
}
```

The following compiler error will be given:
```
error: variable type 'Base' is an abstract class
```

To demonstrate `Derived` cannot be instantiated if `foo()` has not been overridden:
- Remove `Derived::foo()`.
- Attempt to compile the program.
```cpp
#include <iostream>

struct Base {
    virtual void foo() = 0;
};

struct Derived : public Base {
};

int main() {
    Derived d;
}
```

The following compiler error will be given:
```
error: variable type 'Derived' is an abstract class
```

---

</p></details>

Modify `BaseRobot` such that it is an abstract class. What is an appropriate function to make pure virtual?

<details><summary>Answer</summary><p>

We want to force the behaviour of the robot to change between `BaseRobot` and `AutonomousRobot`, so we should mark the control loop, `run()`, as pure virtual.

```cpp
class BaseRobot {
public:
    BaseRobot(int);
    virtual void run() = 0;  // Pure virtual function.
    void halt();
    void moveForward();
    void moveBackward();
    void turnLeft();
    void turnRight();

protected:
    void wait(double);

    static constexpr double mMaxMotorSpeed{6.28};
    int const mDuration;

    webots::Robot mController;
    webots::Motor& mLeftMotor;
    webots::Motor& mRightMotor;
    webots::Keyboard& mKeyboard;
};
```

`BaseRobot` can no longer be instantiated so now it makes no sense to have the `base_robot` controller.

`AutonomousRobot` and `autonomous_robot` does not need to be modified:
```cpp
class AutonomousRobot : public BaseRobot {
public:
    AutonomousRobot(int);
    virtual void run() override;  // Overriding pure virtual function.
};
```

You'll find the behaviour is still the same as [Exercise: Virtual Functions](#exercise-virtual-functions). The only difference is that `base_robot` can no longer be used.

---

</p></details>

---

## Exercise: Virtual Destructor

Consider the following program which shows the order of construction and destruction via an up-casted pointer:
```cpp
#include <iostream>

struct Base {
    Base() { std::cout << "Creating Base." << std::endl; }
    ~Base() { std::cout << "Destroying Base." << std::endl; }
};

struct Derived : public Base {
    Derived() { std::cout << "Creating Derived." << std::endl; }
    ~Derived() { std::cout << "Destroying Derived." << std::endl; }
};

int main() {
    Base* b = new Derived;  // Up-cast.
    delete b;
}
```

What is the output of the program?

<details><summary>Answer</summary><p>

```
Creating Base.
Creating Derived.
Destroying Base.
```

`"Destroying Derived."` is never outputted which indicates that `Derived::~Derived()` is never called. This can potentially induce a memory leak.

---

</p></details>

Modify the program so that `Base::~Base()` is marked `virtual`. What is the output of the program?

<details><summary>Answer</summary><p>

If `~Base()` is marked `virtual`:
```cpp
struct Base {
    Base() { std::cout << "Creating Base." << std::endl; }
    virtual ~Base() { std::cout << "Destroying Base." << std::endl; }
};
```

The output is now:
```
Creating Base.
Creating Derived.
Destroying Derived.
Destroying Base.
```

The memory leak has been handled. It is good practice to always mark the destructors of the base class as `virtual` to avoid this memory leak.

This is just a bad quirk of C++ that we as the programmer needs to consider.

---

</p></details>

---

## Feedback

If you liked or disliked anything about the labs, please leave some [feedback](https://forms.office.com/r/sV4X0xR7dT)!
