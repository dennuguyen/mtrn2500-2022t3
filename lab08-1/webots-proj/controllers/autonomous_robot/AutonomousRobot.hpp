#pragma once

#include "BaseRobot.hpp"

class AutonomousRobot : public BaseRobot {
public:
    AutonomousRobot(int);
    // void run();
    virtual void run() override;  // Virtual function.
};