#include "AutonomousRobot.hpp"
// #include "BaseRobot.hpp"  // Used for dynamic casting.

int main(int argc, char** argv) {
    {  // Encapsulating classes.
        AutonomousRobot r(64);
        r.run();
    }

    {  // Up-casting.
        AutonomousRobot r(64);
        BaseRobot* b{dynamic_cast<BaseRobot*>(&r)};
        b->run();
    }
}
