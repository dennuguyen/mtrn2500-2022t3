#include "AutonomousRobot.hpp"

AutonomousRobot::AutonomousRobot(int duration) : BaseRobot(duration) {}

void AutonomousRobot::run() {
    while (mController.step(mDuration) != -1) {
        moveForward();
        wait(1000);
        moveBackward();
        wait(1000);
        turnLeft();
        wait(500);
    }
}
