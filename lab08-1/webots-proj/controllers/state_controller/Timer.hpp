#pragma once

#include <iostream>

#include "webots/Robot.hpp"

namespace mtrn2500 {

class Timer {
public:
    Timer(webots::Robot const& robot) : mRobot(robot) {}

    void time(double const duration) {
        mDuration = duration / 1000;
        mStart = mRobot.getTime();
    }

    bool expired() const { return mRobot.getTime() > mStart + mDuration; }

    void wait(double const duration) {
        time(duration);
        while (!expired()) {
        }
    }

    friend std::ostream& operator<<(std::ostream& os, Timer const& timer) {
        os << "Timer - ";
        os << "start: " << timer.mStart << " - ";
        os << "duration: " << timer.mDuration << " - ";
        os << "current: " << timer.mRobot.getTime();
        return os;
    }

private:
    webots::Robot const& mRobot;
    double mStart;
    double mDuration;
};

}  // namespace mtrn2500