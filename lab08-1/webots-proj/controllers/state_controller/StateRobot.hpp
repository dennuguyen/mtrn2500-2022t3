#pragma once

#include "BaseRobot.hpp"

class StateRobot : public BaseRobot {
public:
    StateRobot(int);
    void run() override;
};