#include "StateRobot.hpp"

#include "Timer.hpp"

StateRobot::StateRobot(int duration) : BaseRobot(duration) {}

void StateRobot::run() {
    bool autoMode{false};
    bool forwardFlag{true};
    mtrn2500::Timer timer(mController);

    while (mController.step(mDuration) != -1) {
        char const key = static_cast<char>(mKeyboard.getKey());
        std::cout << key << std::endl;
        switch (key) {
            case 'Q':
                halt();
                return;
            case 'E':
                autoMode = !autoMode;
                halt();
                timer.time(0);
        }

        if (autoMode) {
            if (timer.expired() && forwardFlag) {
                moveForward();
                timer.time(1000);
                forwardFlag = !forwardFlag;
            }
            if (timer.expired() && !forwardFlag) {
                moveBackward();
                timer.time(1000);
                forwardFlag = !forwardFlag;
            }
        } else {
            switch (key) {
                case 'W':
                    moveForward();
                    break;
                case 'A':
                    turnLeft();
                    break;
                case 'S':
                    moveBackward();
                    break;
                case 'D':
                    turnRight();
                    break;
                case ' ':
                    halt();
                    break;
            }
        }
    }
}