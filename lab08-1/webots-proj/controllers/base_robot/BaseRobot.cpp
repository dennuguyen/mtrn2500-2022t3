#include "BaseRobot.hpp"

#include <iostream>

BaseRobot::BaseRobot(int duration)
    : mDuration(duration),
      mController(webots::Robot()),
      mLeftMotor(*mController.getMotor("left wheel motor")),
      mRightMotor(*mController.getMotor("right wheel motor")),
      mKeyboard(*mController.getKeyboard()) {
    mKeyboard.enable(mDuration);
    halt();
}

void BaseRobot::run() {
    while (mController.step(mDuration) != -1) {
        char const key = static_cast<char>(mKeyboard.getKey());
        std::cout << key << std::endl;
        switch (key) {
            case 'W':
                moveForward();
                break;
            case 'A':
                turnLeft();
                break;
            case 'S':
                moveBackward();
                break;
            case 'D':
                turnRight();
                break;
            case ' ':
                halt();
                break;
        }
    }
}

void BaseRobot::halt() {
    mLeftMotor.setVelocity(0);
    mRightMotor.setVelocity(0);
}

void BaseRobot::moveForward() {
    mLeftMotor.setPosition(INFINITY);
    mRightMotor.setPosition(INFINITY);
    mLeftMotor.setVelocity(mMaxMotorSpeed);
    mRightMotor.setVelocity(mMaxMotorSpeed);
}

void BaseRobot::moveBackward() {
    mLeftMotor.setPosition(INFINITY);
    mRightMotor.setPosition(INFINITY);
    mLeftMotor.setVelocity(-mMaxMotorSpeed);
    mRightMotor.setVelocity(-mMaxMotorSpeed);
}

void BaseRobot::turnLeft() {
    mLeftMotor.setPosition(INFINITY);
    mRightMotor.setPosition(INFINITY);
    mLeftMotor.setVelocity(-mMaxMotorSpeed / 2);
    mRightMotor.setVelocity(mMaxMotorSpeed / 2);
}

void BaseRobot::turnRight() {
    mLeftMotor.setPosition(INFINITY);
    mRightMotor.setPosition(INFINITY);
    mLeftMotor.setVelocity(mMaxMotorSpeed / 2);
    mRightMotor.setVelocity(-mMaxMotorSpeed / 2);
}

void BaseRobot::wait(double waitPeriod) {
    const double start{mController.getTime()};
    while (mController.getTime() - start < waitPeriod * 0.001) {
        mController.step(mDuration);
    }
}
