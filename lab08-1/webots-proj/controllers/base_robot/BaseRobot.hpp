#pragma once

#include "webots/Keyboard.hpp"
#include "webots/Motor.hpp"
#include "webots/Robot.hpp"

class BaseRobot {
public:
    BaseRobot(int);
    // void run();
    // virtual void run();  // Virtual function.
    virtual void run() = 0;  // Pure virtual function.
    void halt();
    void moveForward();
    void moveBackward();
    void turnLeft();
    void turnRight();

protected:
    void wait(double);

    static constexpr double mMaxMotorSpeed{6.28};
    int const mDuration;

    webots::Robot mController;
    webots::Motor& mLeftMotor;
    webots::Motor& mRightMotor;
    webots::Keyboard& mKeyboard;
};