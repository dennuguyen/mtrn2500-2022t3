# MTRN2500 - Lab08-1 (Webots II)

## Learning Outcomes

- Know the main components of a state diagram.
- Know how to approach software design problems.
- Can translate a state diagram to code implementation.

---

## Drawing a State Diagram

State diagrams are used to describe the behaviour of a system.

The most important components of a state diagram consists of:
- **States** are represented as boxes. A state usually describes an action that the system is doing.
    ```mermaid
    stateDiagram-v2
        A
    ```

- **Transitions** are represented as arrows. A transition is always required to move from one state to another.
    ```mermaid
    stateDiagram-v2
        A --> B
        A
        B
    ```

- **Forks** are represented as diamonds. Forks describe the `if-else` statement i.e. the ability to branch.
    ```mermaid
    stateDiagram-v2
        state my_if_else <<choice>>
        my_if_else --> A: Go to A if ...
        my_if_else --> B: Go to B if ...
    ```
    It is good practice to label your transitions to define when this transition occurs.

> You may use other shapes to represent components of your state diagram, just ensure consistency.

There are a variety of tools that you can use to create state diagrams:
- [Mermaid](https://mermaid-js.github.io/mermaid/#/).
- [draw.io](https://app.diagrams.net/).
- [Lucidchart](https://www.lucidchart.com/pages/).

<details><summary>Answer</summary><p>

State diagrams are informally used in MTRN2500. This is just a tool that will help design a software system using a top-down approach.

---

</p></details>

---

## Exercise: Designing a State Diagram

Robot software design can be approached top-down i.e. identifying the problem requirements, defining the robot functionality (with the help of a diagram or interface), then implementing the functionality.

Design and draw a state diagram which solves the following problem:

> A palletiser is required to pick and place some unknown number of parcels from zone A to zone B. Zone A uses a scale sensor to indicate if there is a parcel in zone A or not.

<details><summary>Answer</summary><p>

The state diagram does not have to be too low level.

A possible state diagram solution may be:
```mermaid
stateDiagram-v2
    [*] --> sense_weight

    state if <<choice>>
    sense_weight --> if
    if --> [*]: If scale weight indicates no parcel

    if --> pick_up: If scale weight indicates a parcel
    pick_up --> move_to_B
    move_to_B --> put_down
    put_down --> move_to_A
    move_to_A --> sense_weight

    sense_weight: Get scale weight
    pick_up: Pick up parcel from zone A
    move_to_B: Move palletiser to zone B
    put_down: Put down parcel in zone B
    move_to_A: Move palletiser to zone A
```

A brute-force thinking process is to start at the entry point i.e.
1. What is the first thing the robot should do?
1. What does the robot do next?
1. Brainstorm and expand on all the possible routes the robot can take.

A backtracking thinking process is to:
1. Start with the states that are required.
1. Create transitions between required states and create more in-between states as needed.

---

</p></details>

---

## Exercise: Implementing a State Diagram

Create a controller, `state_controller`, with a class, `StateRobot`, which inherits `BaseRobot`. Override `StateRobot::run()` so the control loop matches the following state diagram.

You may only have one `webots::Robot::step()` loop.

> It may be helpful to use flags to indicate the current mode and non-blocking timer (given) to control transition between states.

```mermaid
stateDiagram-v2
    [*] --> input

    state if <<choice>>
    input --> if

    if --> teleop: If auto mode disabled
    teleop --> input

    if --> disable_auto:  If 'e' pressed and auto mode enabled
    disable_auto --> halt_after_input
    halt_after_input --> input

    if --> auto: If auto mode enabled
    auto --> input

    if --> enable_auto: If 'e' pressed and auto mode disabled
    enable_auto --> halt_after_input
    halt_after_input --> input

    if --> Halt: If 'q' pressed
    Halt --> [*]

    teleop: Teleoperation mode
    state teleop {
        state if_teleop <<choice>>
        [*] --> if_teleop
        if_teleop --> w: If 'w' pressed
        if_teleop --> a: If 'a' pressed
        if_teleop --> s: If 's' pressed
        if_teleop --> d: If 'd' pressed
        state end_teleop <<join>>
        w --> end_teleop
        a --> end_teleop
        s --> end_teleop
        d --> end_teleop
        end_teleop --> [*]

        w: Move forward
        a: Move backward
        s: Rotate left
        d: Rotate right
    }

    auto: Autonomous mode
    state auto {
        [*] --> forward
        forward --> backward
        backward --> forward
        forward: Move forward for 1 s
        backward: Move backward for 1 s
    }

    halt_after_input: Halt
    input: Get key input
    disable_auto: Disable auto mode
    enable_auto: Enable auto mode
```

<details><summary>Answer</summary><p>

There are many different ways of implementing the state diagram.

Start with getting the key input since all branches enter and leave this node.
```cpp
void StateRobot::run() {
    while (mController.step(mDuration) != -1) {
        // Try and get a key press with each loop.
        char const key = static_cast<char>(mKeyboard.getKey());
        std::cout << key << std::endl;
    }
}
```

Implement a `switch-case` to handle `q` and `e` being pressed.
```cpp
void StateRobot::run() {
    bool autoMode{false};  // A flag is required to handle auto mode enabled/disabled states.

    while (mController.step(mDuration) != -1) {
        char const key = static_cast<char>(mKeyboard.getKey());
        std::cout << key << std::endl;

        // Switch-case for only quitting and toggling auto mode.
        switch (key) {
            case 'Q':
                halt();
                return;
            case 'E':
                autoMode = !autoMode;
                halt();
        }
    }
}
```

Implement the autonomous mode state. A naive solution may look like this:
```cpp
void StateRobot::run() {
    bool autoMode{false};
    bool forwardFlag{true};
    mtrn2500::Timer timer(mController);

    while (mController.step(mDuration) != -1) {
        char const key = static_cast<char>(mKeyboard.getKey());
        std::cout << key << std::endl;
        switch (key) {
            case 'Q':
                halt();
                return;
            case 'E':
                autoMode = !autoMode;
                halt();
                timer.time(0);
        }

        // Autonomous mode state.
        if (autoMode) {
            moveForward();
            wait(1000);
            moveBackward();
            wait(1000);
        }
    }
}
```

The autonomous mode is achieved however the downside of using a blocking timer is that it's hard to capture the key input. The blocking timer will block us from getting the key.

`mtrn2500::Timer` is a non-blocking timer which will still allow key input while the robot is moving:
- Two non-blocking timers can be used to ensure the move forward and backward for `1 s`.
- Alternatively, a flag and a single non-blocking timer can be used.
```cpp
void StateRobot::run() {
    bool autoMode{false};
    bool forwardFlag{true};  // Flag to indicate if moving forward or backward.
    mtrn2500::Timer timer(mController);

    while (mController.step(mDuration) != -1) {
        char const key = static_cast<char>(mKeyboard.getKey());
        std::cout << key << std::endl;
        switch (key) {
            case 'Q':
                halt();
                return;
            case 'E':
                autoMode = !autoMode;
                halt();
                timer.time(0);  // Set this so timer.expired() will immediately be true.
        }

        if (autoMode) {
            // If moving forward and timer has expired. Then move forward for 1 s.
            if (timer.expired() && forwardFlag) {
                moveForward();
                timer.time(1000);
                forwardFlag = !forwardFlag;
            }
            // If moving backward and timer has expired. Then move forward for 1 s.
            if (timer.expired() && !forwardFlag) {
                moveBackward();
                timer.time(1000);
                forwardFlag = !forwardFlag;
            }
        }
    }
}
```

Finally, implement the teleoperation mode. It is simply an `else` statement from checking `autoMode`.
```cpp
void StateRobot::run() {
    bool autoMode{false};
    bool forwardFlag{true};
    mtrn2500::Timer timer(mController);

    while (mController.step(mDuration) != -1) {
        char const key = static_cast<char>(mKeyboard.getKey());
        std::cout << key << std::endl;
        switch (key) {
            case 'Q':
                halt();
                return;
            case 'E':
                autoMode = !autoMode;
                halt();
                timer.time(0);
        }

        if (autoMode) {
            if (timer.expired() && forwardFlag) {
                moveForward();
                timer.time(1000);
                forwardFlag = !forwardFlag;
            }
            if (timer.expired() && !forwardFlag) {
                moveBackward();
                timer.time(1000);
                forwardFlag = !forwardFlag;
            }
        } else {  // Teleoperation mode.
            switch (key) {
                case 'W':
                    moveForward();
                    break;
                case 'A':
                    turnLeft();
                    break;
                case 'S':
                    moveBackward();
                    break;
                case 'D':
                    turnRight();
                    break;
                case ' ':
                    halt();
                    break;
            }
        }
    }
}
```

---

</p></details>


---

## Feedback

If you liked or disliked anything about the labs, please leave some [feedback](https://forms.office.com/r/sV4X0xR7dT)!
