# MTRN2500 - Lab08-1 (Webots II)

## Learning Outcomes

- Know the main components of a state diagram.
- Know how to approach software design problems.
- Can translate a state diagram to code implementation.

---

## Drawing a State Diagram

State diagrams are used to describe the behaviour of a system.

The most important components of a state diagram consists of:
- **States** are represented as boxes. A state usually describes an action that the system is doing.
    ```mermaid
    stateDiagram-v2
        A
    ```

- **Transitions** are represented as arrows. A transition is always required to move from one state to another.
    ```mermaid
    stateDiagram-v2
        A --> B
        A
        B
    ```

- **Forks** are represented as diamonds. Forks describe the `if-else` statement i.e. the ability to branch.
    ```mermaid
    stateDiagram-v2
        state my_if_else <<choice>>
        my_if_else --> A: Go to A if ...
        my_if_else --> B: Go to B if ...
    ```
    It is good practice to label your transitions to define when this transition occurs.

> You may use other shapes to represent components of your state diagram, just ensure consistency.

There are a variety of tools that you can use to create state diagrams:
- [Mermaid](https://mermaid-js.github.io/mermaid/#/).
- [draw.io](https://app.diagrams.net/).
- [Lucidchart](https://www.lucidchart.com/pages/).



---

## Exercise: Designing a State Diagram

Robot software design can be approached top-down i.e. identifying the problem requirements, defining the robot functionality (with the help of a diagram or interface), then implementing the functionality.

Design and draw a state diagram which solves the following problem:

> A palletiser is required to pick and place some unknown number of parcels from zone A to zone B. Zone A uses a scale sensor to indicate if there is a parcel in zone A or not.



---

## Exercise: Implementing a State Diagram

Create a controller, `state_controller`, with a class, `StateRobot`, which inherits `BaseRobot`. Override `StateRobot::run()` so the control loop matches the following state diagram.

You may only have one `webots::Robot::step()` loop.

> It may be helpful to use flags to indicate the current mode and non-blocking timer (given) to control transition between states.

```mermaid
stateDiagram-v2
    [*] --> input

    state if <<choice>>
    input --> if

    if --> teleop: If auto mode disabled
    teleop --> input

    if --> disable_auto:  If 'e' pressed and auto mode enabled
    disable_auto --> halt_after_input
    halt_after_input --> input

    if --> auto: If auto mode enabled
    auto --> input

    if --> enable_auto: If 'e' pressed and auto mode disabled
    enable_auto --> halt_after_input
    halt_after_input --> input

    if --> Halt: If 'q' pressed
    Halt --> [*]

    teleop: Teleoperation mode
    state teleop {
        state if_teleop <<choice>>
        [*] --> if_teleop
        if_teleop --> w: If 'w' pressed
        if_teleop --> a: If 'a' pressed
        if_teleop --> s: If 's' pressed
        if_teleop --> d: If 'd' pressed
        state end_teleop <<join>>
        w --> end_teleop
        a --> end_teleop
        s --> end_teleop
        d --> end_teleop
        end_teleop --> [*]

        w: Move forward
        a: Move backward
        s: Rotate left
        d: Rotate right
    }

    auto: Autonomous mode
    state auto {
        [*] --> forward
        forward --> backward
        backward --> forward
        forward: Move forward for 1 s
        backward: Move backward for 1 s
    }

    halt_after_input: Halt
    input: Get key input
    disable_auto: Disable auto mode
    enable_auto: Enable auto mode
```




---

## Feedback

If you liked or disliked anything about the labs, please leave some [feedback](https://forms.office.com/r/sV4X0xR7dT)!
