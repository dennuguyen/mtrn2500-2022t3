#include "webots/DistanceSensor.hpp"
#include "webots/Robot.hpp"

constexpr int duration{64};                    // Robot cycle duration.
constexpr double distanceSensorDuration{120};  // How many ms before the sensor refreshes.

void wait(webots::Robot& robot, double waitPeriod) {
    const double start{robot.getTime()};
    while (robot.getTime() - start < waitPeriod * 0.001) {
        robot.step(duration);
    }
}

int main() {
    // Instantiate devices.
    webots::Robot controller;
    webots::DistanceSensor& ps1{*controller.getDistanceSensor("ps1")};

    // Enable distance sensor.
    ps1.enable(distanceSensorDuration);

    // {  // NaN because controller does not refresh/step.
    //     double val{ps1.getValue()};
    //     std::cout << val << std::endl;
    // }

    // {  // NaN because sensor refreshes after controller refreshes.
    //     controller.step(100);
    //     double val{ps1.getValue()};
    //     std::cout << val << std::endl;
    // }

    // {  // Gives a value because sensor refreshes before controller refreshes.
    //     controller.step(120);
    //     double val{ps1.getValue()};
    //     std::cout << val << std::endl;
    // }

    // {  // Wait before running.
    //     wait(controller, 100);
    //     double val{ps1.getValue()};
    //     std::cout << val << std::endl;
    // }

    {  // Convert value to metres.
        wait(controller, 100);
        while (controller.step(duration) != -1) {
            double const threshold{306};
            double val{ps1.getValue()};
            if (val > threshold) {
                std::cout << val << " > " << threshold << " is close!";
            } else {
                std::cout << val << " < " << threshold << " is far!";
            }
        }
    }
}