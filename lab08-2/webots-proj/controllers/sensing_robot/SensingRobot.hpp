#pragma once

#include <array>

#include "BaseRobot.hpp"
#include "webots/DistanceSensor.hpp"

class SensingRobot : public BaseRobot {
public:
    SensingRobot(int);
    void run() override;

private:
    std::array<bool, 4> detectObstacles() const;

    static bool isClose(double value);

    static constexpr int mNumDistanceSensors{8};

    const std::array<std::string, mNumDistanceSensors> mSensorNames = {
        "ps0", "ps1", "ps2", "ps3", "ps4", "ps5", "ps6", "ps7",
    };

    std::array<webots::DistanceSensor*, mNumDistanceSensors> mSensors;
};