#include "SensingRobot.hpp"

SensingRobot::SensingRobot(int duration) : BaseRobot(duration) {
    for (int i{0}; i < mNumDistanceSensors; i++) {
        mSensors[i] = mController.getDistanceSensor(mSensorNames[i]);
        mSensors[i]->enable(mDuration);
    }
}

void SensingRobot::run() {
    while (mController.step(mDuration) != -1) {
        teleoperate();
        for (auto const& i : detectObstacles()) {
            std::cout << std::boolalpha << i << " ";
        }
        std::cout << std::endl;
    }
}

std::array<bool, 4> SensingRobot::detectObstacles() const {
    std::array<bool, 4> obstacles{
        isClose(mSensors[0]->getValue()) || isClose(mSensors[7]->getValue()),
        isClose(mSensors[5]->getValue()),
        isClose(mSensors[2]->getValue()),
        isClose(mSensors[3]->getValue()) || isClose(mSensors[4]->getValue()),
    };
    return obstacles;
}

bool SensingRobot::isClose(double value) { return value > 306; }
