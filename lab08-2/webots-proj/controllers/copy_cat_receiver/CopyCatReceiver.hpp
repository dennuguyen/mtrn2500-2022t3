#pragma once

#include "BaseRobot.hpp"
#include "webots/Receiver.hpp"

class CopyCatReceiver : public BaseRobot {
public:
    CopyCatReceiver(int);
    void run() override;

private:
    webots::Receiver& mReceiver;
};
