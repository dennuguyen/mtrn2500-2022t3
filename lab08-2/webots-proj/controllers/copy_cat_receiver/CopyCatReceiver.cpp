#include "CopyCatReceiver.hpp"

CopyCatReceiver::CopyCatReceiver(int duration)
    : BaseRobot(duration), mReceiver(*mController.getReceiver("receiver")) {
    mReceiver.enable(mDuration);
}

void CopyCatReceiver::run() {
    char command;
    std::string const validCommands("WASD ");
    while (mController.step(mDuration) != -1) {
        if (mReceiver.getQueueLength() > 0) {
            // Convert received data from (void*) to (const char*) to (std::string).
            std::string data{static_cast<const char*>(mReceiver.getData())};
            mReceiver.nextPacket();

            // Since data is in ASCII, convert from (std::string) to (int) to (char).
            command = static_cast<char>(std::stoi(data));

            std::cout << "RECEIVER: " << command << std::endl;
        }

        switch (command) {
            case 'W':
                moveBackward();
                break;
            case 'A':
                turnRight();
                break;
            case 'S':
                moveForward();
                break;
            case 'D':
                turnLeft();
                break;
            case ' ':
                halt();
                break;
        }
    }
}
