#include <iostream>
#include <string>

// Webots.
#include "webots/Receiver.hpp"
#include "webots/Robot.hpp"

constexpr int duration{64};  // Robot cycle duration.

int main() {
    webots::Robot controller;
    webots::Receiver& receiver{*controller.getReceiver("receiver")};
    receiver.enable(duration);  // Turn on the device.
    receiver.setChannel(2);     // Set the channel. Default is 0.
    while (controller.step(duration) != -1) {
        if (receiver.getQueueLength() > 0) {  // Only get data if data exists in the queue.
            std::string data{
                static_cast<const char*>(receiver.getData())};  // Get the data at front of queue.
            receiver.nextPacket();                              // Pop the queue.
            std::cout << "Receiver: " << data << std::endl;
        }
    }
}