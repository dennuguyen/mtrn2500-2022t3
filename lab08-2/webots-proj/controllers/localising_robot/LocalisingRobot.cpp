#include "LocalisingRobot.hpp"

#include <algorithm>
#include <array>
#include <iomanip>

LocalisingRobot::LocalisingRobot(int duration)
    : BaseRobot(duration),
      mGPS(*mController.getGPS("gps")),
      mCompass(*mController.getCompass("compass")) {
    // Enable GPS and compass in constructor before devices are used.
    mGPS.enable(duration);
    mCompass.enable(duration);
}

void LocalisingRobot::run() {
    while (mController.step(mDuration) != -1) {
        teleoperate();

        // Convert from (const double*) to (std::array) because we should never deal with C-style
        // arrays directly.
        const double* gpsValuesRetrieved(mGPS.getValues());
        std::array<double, 3> gpsValues;
        std::copy(gpsValuesRetrieved, gpsValuesRetrieved + 3, gpsValues.begin());

        const double* compassValuesRetrieved(mCompass.getValues());
        std::array<double, 3> compassValues;
        std::copy(compassValuesRetrieved, compassValuesRetrieved + 3, compassValues.begin());

        // Students don't have to worry about iomanip.
        std::cout << std::fixed << std::setprecision(3) << "(x: " << std::setw(6) << gpsValues[0]
                  << ", y: " << std::setw(6) << gpsValues[1] << ", z: " << std::setw(6)
                  << gpsValues[2] << ", x-a: " << std::setw(6) << compassValues[0]
                  << ", y-a: " << std::setw(6) << compassValues[1] << ", z-a: " << std::setw(6)
                  << compassValues[2] << ")" << std::endl;
    }
}
