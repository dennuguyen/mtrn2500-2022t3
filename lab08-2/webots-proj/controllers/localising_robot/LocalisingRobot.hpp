#pragma once

#include "BaseRobot.hpp"
#include "webots/Compass.hpp"
#include "webots/GPS.hpp"
#include "webots/Robot.hpp"

class LocalisingRobot : public BaseRobot {
public:
    LocalisingRobot(int);
    void run() override;

private:
    webots::GPS& mGPS;
    webots::Compass& mCompass;
};