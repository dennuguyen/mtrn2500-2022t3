#pragma once

#include "BaseRobot.hpp"
#include "webots/Emitter.hpp"

class CopyCatEmitter : public BaseRobot {
public:
    CopyCatEmitter(int);
    void run() override;

private:
    webots::Emitter& mEmitter;
};