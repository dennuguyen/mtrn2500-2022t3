#include "CopyCatEmitter.hpp"

#include <string>

CopyCatEmitter::CopyCatEmitter(int duration)
    : BaseRobot(duration), mEmitter(*mController.getEmitter("emitter")) {}

void CopyCatEmitter::run() {
    char command;
    std::string const validCommands("WASD ");
    while (mController.step(mDuration) != -1) {
        char key{teleoperate()};

        // Check if key is valid or else garbage is sent.
        if (validCommands.find(key) != std::string::npos) {
            command = key;
            std::string data{std::to_string(command)};
            mEmitter.send(data.c_str(), data.size());

            std::cout << "EMITTER: " << command << std::endl;
        }
    }
}
