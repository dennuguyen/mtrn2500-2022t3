#include <iostream>
#include <string>

// Webots.
#include "webots/Emitter.hpp"
#include "webots/Robot.hpp"

constexpr int duration{64};  // Robot cycle duration.

int main() {
    webots::Robot controller;
    webots::Emitter& emitter{*controller.getEmitter("emitter")};
    emitter.setChannel(2);  // Set the channel. Default is 0.
    int i{0};
    while (controller.step(duration) != -1) {
        std::string data{std::to_string(i)};
        emitter.send(data.c_str(), data.size());  // Send data.
        i++;
        std::cout << "Emitter: " << i << std::endl;
    }
}