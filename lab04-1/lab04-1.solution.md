# MTRN2500 - Lab04-1 (STL & Exceptions)

## Learning Outcomes

- Exception handling.
- Iterators.
- STL algorithms.
- Lambda functions.
- STL containers.
- Iterator adaptors.

---

## Revision: EOF

What is an EOF? What is it used for?

<details><summary>Answer</summary><p>

EOF stands for end of file and should be used to end a buffer.

In particular, we use EOF to stop reading from `std::cin`.

---

</p></details>

What character inputs would cause an EOF?

<details><summary>Answer</summary><p>

On Unix machines, `ctrl + d` will cause an EOF.

On Windows, `ctrl + z` will cause an EOF.

---

</p></details>

> Note that EOF will be important for today's exercises.

---

## Exercise: Exception Handling

<!-- 

Do not go through nested exception handling.

 -->

What are exceptions?

<details><summary>Answer</summary><p>

Exceptions are errors that occur during runtime.

---

</p></details>

Fill out the following table to discern when different exception types should be thrown.

<table>
    <tr>
        <th>Exception Type</th>
        <th>When to throw</th>
    </tr>
    <tr>
        <td><code>std::out_of_range</code></td>
        <td></td>
    </tr>
    <tr>
        <td><code>std::invalid_argument</code></td>
        <td></td>
    </tr>
    <tr>
        <td><code>std::runtime_error</code></td>
        <td></td>
    </tr>
    <tr>
        <td><code>std::logic_error</code></td>
        <td></td>
    </tr>
</table>

<details><summary>Answer</summary><p>

Consult the documentation: https://en.cppreference.com/w/cpp/error/exception.

<table>
    <tr>
        <th>Exception Type</th>
        <th>When to throw</th>
    </tr>
    <tr>
        <td><code>std::out_of_range</code></td>
        <td>Exceptions that occur due to attempting to access elements outside of a valid range.</td>
    </tr>
    <tr>
        <td><code>std::invalid_argument</code></td>
        <td>Exceptions that occur because an argument value is not acceptable.</td>
    </tr>
    <tr>
        <td><code>std::runtime_error</code></td>
        <td>Exceptions that occur during runtime, not easily predicted, and beyond the scope of the program.</td>
    </tr>
    <tr>
        <td><code>std::logic_error</code></td>
        <td>Exceptions that occur due to violating logical preconditions or invariants.</td>
    </tr>
</table>

We only throw exceptions if we cannot handle the error locally i.e. if we throw, then there will be code higher up that will catch the exception and handle it.

`std::out_of_range` is a type of `std::logic_error`. If we can throw `std::out_of_range` or `std::logic_error`, then prefer to throw `std::out_of_range` because it is more descriptive.

---

</p></details>

The following program contains an `assign()` function which throws `std::runtime_error` if the user tries to assign `var` with an even `value`. This exception currently crashes the program. Modify the following program so that if the exception is thrown, `cleanup()` is applied on the faulty variable, and the program does not crash.

Do not modify `assign()` and `cleanup()`.
```cpp
#include <stdexcept>

void assign(int& var, int const& value) {
    if (value % 2 == 0) {
        throw std::runtime_error("Illegal assignment.");
    }
    var = value;
}

void cleanup(int& var) {
    var = 0;
}

int main() {
    int a{0};
    int b{0};

    // MODIFY BELOW HERE.
    assign(a, 42);
    assign(b, 31);
}
```

<details><summary>Answer</summary><p>

```cpp
#include <stdexcept>

void assign(int& var, int const& value) {
    if (value % 2 == 0) {
        throw std::runtime_error("Illegal assignment.");
    }
    var = value;
}

void cleanup(int& var) {
    var = 0;
}

int main() {
    int a{0};
    int b{0};

    try {
        assign(a, 42);
    } catch (std::runtime_error const& e) {  // Prefer this.
        cleanup(a);
    }

    try {
        assign(b, 31);
    } catch (std::exception const& e) {  // Catches all std::exception. Sorta bad style.
        cleanup(b);
    } catch (...) {  // Catches all exceptions. Very bad style.
        cleanup(b);
    }
}
```

The converted code now does not crash when we do an illegal assignment, catches the thrown exception, and cleans up the variable.

Points of change:
- Since we need to catch the exception, we place `assign` in the `try` block.
- We then call `cleanup` in the `catch` block if an exception is indeed thrown.
- We must always make sure caught exceptions, `e`, are `const&`.
- We should always try and be specific with the type of exception caught, unless we really want to catch any type of exception.

---

</p></details>

---

## Exercise: Iterators

What is an iterator?

<details><summary>Answer</summary><p>

Iterators are classes that encapsulate raw pointers and provides methods to interact with the pointer e.g. incrementing and derefencing. Consider the documentation for `reverse_iterator`: https://en.cppreference.com/w/cpp/iterator/reverse_iterator. We can see defined functions such as:
- `operator*`
- `operator[]`
- `operator++`

These are the same operators that exists for raw pointers back in C so nothing is new.

---

</p></details>

Where in `std::vector` does `std::vector::begin()` and `std::vector::end()` point to?

<details><summary>Answer</summary><p>

This is a small revision question.

`begin()` points to the first element of the container.

`end()` points to the one-after-the-last element of the container.

---

</p></details>

How would one obtain the datatype of the iterator for `std::vector<int>`?

<details><summary>Answer</summary><p>

Consult the documentation: https://en.cppreference.com/w/cpp/container/vector. There is a member type called `iterator`.

```cpp
std::vector<int>::iterator it;
```

---

</p></details>

Write a program that uses iterators in a for-loop to print each element of `std::set`.

```cpp
#include <iostream>
#include <set>

int main() {
    std::set<double> s{0.2, 1.41, 12.2, 43.3};

    // WRITE YOUR SOLUTION HERE.
}
```

<details><summary>Answer</summary><p>

Use the answers to the above questions to answer this.

```cpp
#include <iostream>
#include <set>

int main() {
    std::set<double> s{0.2, 1.41, 12.2, 43.3};

    for (std::set<double>::iterator it{s.begin()}; it != s.end(); it++) {
        std::cout << *it << " ";
    }
    std::cout << std::endl;
}
```

Or less verbose is to use `auto`:
```cpp
for (auto it{s.begin()}; it != s.end(); it++)
```

- Iterators can be assigned: `it = s.begin()`.
- Iterators can be compared with other iterators: `it != s.end()`.
- Iterators can be incremented to obtain the next iterator: `it++`.
- Iterators can be dereferenced: `*it`.

---

</p></details>

---

## Iterator Types

C++14 has five types of iterators. Fill out the following table to compare their differences.

<table>
    <tr>
        <th>Iterator Type</th>
        <th>Read/Write Operation</th>
        <th>Read/Write Access Order</th>
    </tr>
    <tr>
        <td><code>InputIterator</code></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><code>OutputIterator</code></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><code>ForwardIterator</code></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><code>BidirectionalIterator</code></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><code>RandomAccessIterator</code></td>
        <td></td>
        <td></td>
    </tr>
</table>

<details><summary>Answer</summary><p>

Consult the documentation: https://en.cppreference.com/w/cpp/iterator. There is a large table which compares the iterator types.

<table>
    <tr>
        <th>Iterator Type</th>
        <th>Read/Write</th>
        <th>Direction</th>
    </tr>
    <tr>
        <td><code>InputIterator</code></td>
        <td>Read</td>
        <td>Forward (single pass)</td>
    </tr>
    <tr>
        <td><code>OutputIterator</code></td>
        <td>Write</td>
        <td>Forward (single pass)</td>
    </tr>
    <tr>
        <td><code>ForwardIterator</code></td>
        <td>Read</td>
        <td>Forward (multiple passes)</td>
    </tr>
    <tr>
        <td><code>BidirectionalIterator</code></td>
        <td>Read</td>
        <td>Forward (multiple passes)<br>Reverse</td>
    </tr>
    <tr>
        <td><code>RandomAccessIterator</code></td>
        <td>Read</td>
        <td>Forward (multiple passes)<br>Reverse<br>Random access</td>
    </tr>
</table>

---

</p></details>

---

## Exercise: STL Algorithms

STL algorithms are convenience functions that lets us interact with STL containers via iterators. There is a list of what STL algorithms C++ provides: https://en.cppreference.com/w/cpp/algorithm.

What is the main header library for most STL algorithms?

<details><summary>Answer</summary><p>

```cpp
#include <algorithm>
```

---

</p></details>

What are typically the first two parameters of an STL algorithm?

<details><summary>Answer</summary><p>

The parameters are `first` and `last` which represents the range of elements `[first, last)` of some STL container to be iterated over.

---

</p></details>

Write a program which uses an STL algorithm to assign every element of `vec` of size `vec_size` with `value`. The first line of input is `vec_size`. The second line of input is `value`.

<table>
    <tr>
        <th>Input</th>
        <th>Output</th>
    </tr>
    <tr>
        <td><pre><code>0
1</code></pre></td>
        <td><pre><code></code></pre></td>
    </tr>
    <tr>
        <td><pre><code>3
4</code></pre></td>
        <td><pre><code>4 4 4</code></pre></td>
    </tr>
    <tr>
        <td><pre><code>5
0</code></pre></td>
        <td><pre><code>0 0 0 0 0</code></pre></td>
    </tr>
</table>

```cpp
#include <algorithm>
#include <iostream>
#include <vector>

int main() {
    // Read in the vector size.
    int vec_size{0};
    std::cin >> vec_size;
    std::vector<int> vec(vec_size);

    // Read in the value.
    int value{0};
    std::cin >> value;

    // WRITE YOUR SOLUTION HERE.

    // To print the vector.
    for (auto const& i : vec) {
        std::cout << i << " ";
    }
    std::cout << std::endl;
}
```

<details><summary>Answer</summary><p>

Consult the documentation: https://en.cppreference.com/w/cpp/algorithm.

One can find an algorithm called `std::fill`.

```cpp
#include <algorithm>
#include <iostream>
#include <vector>

int main() {
    // Read in the vector size.
    int vec_size{0};
    std::cin >> vec_size;
    std::vector<int> vec(vec_size);

    // Read in the value.
    int value{0};
    std::cin >> value;

    std::fill(vec.begin(), vec.end(), value);

    // To print the vector.
    for (auto const& i : vec) {
        std::cout << i << " ";
    }
    std::cout << std::endl;
}
```

---

</p></details>

---

## Exercise: Lambda Functions

How do we change the default behaviour of an STL algorithm?

<details><summary>Answer</summary><p>

We can change the default behaviour of an STL algorithm with a lambda function.

A lot of STL algorithms will have a function overload for accepting a **predicate**, **operation**, or **function** function:
- Predicates are true/false expressions.
- Operations modify the given inputs.

These functions can have one or two inputs:
- Single inputs work on one element at a time within a range.
- Two inputs work on two consecutive elements at a time within a range.

<table>
    <tr>
        <th>Return Type</th>
        <th>Single Input</th>
        <th>Two Inputs</th>
    </tr>
    <tr>
        <td>Boolean</td>
        <td>UnaryPredicate</td>
        <td>BinaryPredicate</td>
    </tr>
    <tr>
        <td>Element</td>
        <td>UnaryOperation</td>
        <td>BinaryOperation</td>
    </tr>
    <tr>
        <td>Void</td>
        <td>UnaryFunction</td>
        <td>BinaryFunction</td>
    </tr>
</table>

For example, look at the signature and implementation of the following STL algorithms:
- [`std::for_each`](https://en.cppreference.com/w/cpp/algorithm/for_each) has a `UnaryFunction` overload.
- [`std::transform`](https://en.cppreference.com/w/cpp/algorithm/transform) has a `UnaryOperation` and `BinaryOperation` overload.
- [`std::find_if`](https://en.cppreference.com/w/cpp/algorithm/find) has a `UnaryPredicate` overload.

---

</p></details>

What is the signature of a lambda function?

<details><summary>Answer</summary><p>

Lambda functions have the following signature:
```cpp
[ capture ]( parameters ){ body }
```
It's easy to remember lambda functions as a set of three brackets:
- The capture brackets allows objects to be passed into the function so they are "scoped" in the function.
- The parameter brackets work the same as function parameters.
- The body brackets work the same as function body.

Lambda functions can also be named:
```cpp
auto my_lambda_function = [](int i){ return i * 2; };
```

---

</p></details>

What is a lambda function?

<details><summary>Answer</summary><p>

Lambda functions are *nameless* functions that are intended to be passed to other functions as an argument.

> More information outside the scope of this course: Lambda functions are syntactic sugar for defining functors which are classes which define `operator()`. Functors are function objects which can be passed to other functions as arguments.

---

</p></details>

Modify the program from **Exercise: Iterators** such that it uses an STL algorithm and lambda function instead.

<details><summary>Answer</summary><p>

```cpp
#include <algorithm>
#include <iostream>
#include <set>

int main() {
    std::set<double> s{0.2, 1.41, 12.2, 43.3};

    std::for_each(s.begin(), s.end(), [](auto const& i){
        std::cout << i << " ";
    });
    std::cout << std::endl;
}
```

---

</p></details>

---

## Exercise: Splitting Pairs

Write a program which uses STL algorithms to split `vec` into `result1` and `result2`.

```cpp
#include <algorithm>
#include <iostream>
#include <utility>
#include <vector>

int main() {
    std::vector<std::pair<std::string, int>> vec{
        {"hello", 12382},
        {"apple", 2310},
        {"triangle", 814},
        {"world", 584},
        {"sentence", 9432},
    };
    std::vector<std::string> result1;
    std::vector<int> result2;

    // WRITE YOUR SOLUTION HERE.

    // To print the results.
    for (auto const& i : result1) {
        std::cout << i << " " << std::endl;
    }
    std::cout << std::endl;

    for (auto const& i : result2) {
        std::cout << i << " " << std::endl;
    }
    std::cout << std::endl;
}
```

<details><summary>Answer</summary><p>

```cpp
#include <algorithm>
#include <iostream>
#include <utility>
#include <vector>

int main() {
    std::vector<std::pair<std::string, int>> vec{
        {"hello", 12382},
        {"apple", 2310},
        {"triangle", 814},
        {"world", 584},
        {"sentence", 9432},
    };
    std::vector<std::string> result1(vec.size());
    std::vector<int> result2(vec.size());

    std::transform(vec.begin(), vec.end(), result1.begin(), [](auto const& i) { return i.first; });
    std::transform(vec.begin(), vec.end(), result2.begin(), [](auto const& i) { return i.second; });

    // To print the results.
    for (auto const& i : result1) {
        std::cout << i << " " << std::endl;
    }
    std::cout << std::endl;

    for (auto const& i : result2) {
        std::cout << i << " " << std::endl;
    }
    std::cout << std::endl;
}
```

- We use `std::transform` since we want to iterate over a range and store the result in another range.
    - https://en.cppreference.com/w/cpp/algorithm/transform
- The lambda function lets us specify which part of the pair we want to return to store in the destination range.

---

</p></details>

---

## Exercise: Counting Upper Case Characters

Write a program which uses an STL algorithm to count how many upper case characters there are in `str`.

<table>
    <tr>
        <th>Input</th>
        <th>Output</th>
    </tr>
    <tr>
        <td><pre><code></code></pre></td>
        <td><pre><code>0</code></pre></td>
    </tr>
    <tr>
        <td><pre><code>ab52nc2b3a</code></pre></td>
        <td><pre><code>0</code></pre></td>
    </tr>
    <tr>
        <td><pre><code>abcDEfg</code></pre></td>
        <td><pre><code>2</code></pre></td>
    </tr>
    <tr>
        <td><pre><code>ABCDEFGHI</code></pre></td>
        <td><pre><code>9</code></pre></td>
    </tr>
</table>

```cpp
#include <algorithm>
#include <iostream>
#include <string>

int main() {
    std::string str;
    std::cin >> str;
    int count{0};

    // WRITE YOUR SOLUTION HERE.

    std::cout << count << std::endl;
}
```

<details><summary>Answer</summary><p>

```cpp
#include <algorithm>
#include <cctype>
#include <iostream>
#include <string>

int main() {
    std::string str;
    std::cin >> str;
    int count{0};

    count = std::count_if(str.begin(), str.end(), [](auto const& i) { return std::isupper(i); });

    // Also acceptable.
    count = std::count_if(str.begin(), str.end(), std::isupper);

    std::cout << count << std::endl;
}
```

- We use `std::count_if` but we need to supply our own lambda function, `std::isupper` to count only the upper case characters.

---

</p></details>

---

## STL Containers

STL containers are convenience classes that stores data of any datatype. There is a list of what STL containers C++ provides: https://en.cppreference.com/w/cpp/container.

Define an appropriate data structure for looking up a person's name via their number ID.

<details><summary>Answer</summary><p>

```cpp
#include <map>

std::map<int, std::string> student_registry = {
    {1, "Jason"},
    {2, "Joanna"},
};

std::cout << student_registry[1] << std::endl;
std::cout << student_registry[2] << std::endl;
```

- A map would make the most sense if we need to "map" a person's name to their ID.
- We also `#include <map>` in order to use the container.

---

</p></details>

Define an appropriate data structure for a line of people waiting to order food at a canteen. Represent people by their names i.e. `std::string`.

<details><summary>Answer</summary><p>

```cpp
#include <queue>

std::queue<std::string> canteen_line;

// Enqueue people waiting to order.
canteen_line.push("Jason");
canteen_line.push("Joanna");

// Pop is a void function so we need to access front to get the element
// before removing from queue.
auto next_person = canteen_line.front();
canteen_line.pop();
```

- A queue would make the most sense to model FIFO behaviour.
- We also `#include <queue>` in order to use the container.

---

</p></details>

---

## Exercise: Counting String Occurrences

Write a program that counts each string occurrence in a vector of strings. The program should output the string in alphabetical order with its count.

Hint: consider defining an STL container to keep track of the string occurrences.

<table>
    <tr>
        <th>Input</th>
        <th>Output</th>
    </tr>
    <tr>
        <td><pre><code></code></pre></td>
        <td><pre><code></code></pre></td>
    </tr>
    <tr>
        <td><pre><code>Apple
Apple
Apple
Apple</code></pre></td>
        <td><pre><code>Apple: 4</code></pre></td>
    </tr>
    <tr>
        <td><pre><code>Apple
Banana
Pear
Apple</code></pre></td>
        <td><pre><code>Apple: 2
Banana: 1
Pear: 1</code></pre></td>
    </tr>
</table>

```cpp
#include <algorithm>
#include <iostream>
#include <map>
#include <string>
#include <vector>

int main() {
    std::string str;
    std::vector<std::string> vec;
    while (std::cin >> str) {
        vec.push_back(str);
    }

    // WRITE YOUR SOLUTION HERE.
}
```

<details><summary>Answer</summary><p>

```cpp
#include <algorithm>
#include <iostream>
#include <map>
#include <string>
#include <vector>

int main() {
    std::string str;
    std::vector<std::string> vec;
    while (std::cin >> str) {
        vec.push_back(str);
    }

    std::map<std::string, int> counts;
    std::for_each(vec.begin(), vec.end(), [&counts](auto const& i) { counts[i]++; });
    std::for_each(counts.begin(), counts.end(), [](auto const& i) { std::cout << i.first << ": " << i.second << std::endl; });
}
```

- A map is used to keep track of the count of each string.
- Note that there are multiple solutions that uses STL algorithms and containers.

---

</p></details>

---

## Iterator Adaptors

What is an iterator adaptor?

<details><summary>Answer</summary><p>

Iterator adaptors convert iterators to other iterator types with different behaviours from the original.

Iterator adaptors change the behaviour of the iterator.

---

</p></details>

Fill out the following table comparing the different types of iterator adaptors. When filling out the *functionality* column, consider the arguments required to create the iterator adaptor.
<table>
    <tr>
        <th>Iterator Adaptor</th>
        <th>Functionality</th>
        <th>Iterator Type</th>
    </tr>
    <tr>
        <td><code>std::reverse_iterator</code></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><code>std::move_iterator</code></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><code>std::front_insert_iterator</code></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><code>std::back_insert_iterator</code></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><code>std::insert_iterator</code></td>
        <td></td>
        <td></td>
    </tr>
</table>

<details><summary>Answer</summary><p>

<table>
    <tr>
        <th>Iterator Adaptor</th>
        <th>Functionality</th>
        <th>Iterator Type</th>
    </tr>
    <tr>
        <td><code>std::reverse_iterator</code></td>
        <td>An iterator with reversed-direction of the given iterator.</td>
        <td><code>BidirectionalIterator</code></td>
    </tr>
    <tr>
        <td><code>std::move_iterator</code></td>
        <td>An iterator with applied move semantics of the given iterator.</td>
        <td><code>InputIterator</code></td>
    </tr>
    <tr>
        <td><code>std::front_insert_iterator</code></td>
        <td>An iterator for prepending elements to the given container.</td>
        <td><code>OutputIterator</code></td>
    </tr>
    <tr>
        <td><code>std::back_insert_iterator</code></td>
        <td>An iterator for appending elements to the given container.</td>
        <td><code>OutputIterator</code></td>
    </tr>
    <tr>
        <td><code>std::insert_iterator</code></td>
        <td>An iterator for inserting elements to the given container at the given iterator's position.</td>
        <td><code>OutputIterator</code></td>
    </tr>
</table>

---

</p></details>

Some STL containers have applied the reverse iterator adaptor for you e.g. vector's [`rbegin`](https://en.cppreference.com/w/cpp/container/vector/rbegin) and [`rend`](https://en.cppreference.com/w/cpp/container/vector/rend).

---

## Exercise: Appending Max Element N Times

Write a program which appends the max element of a given container for another `n` number of times.

You may only use STL algorithms.

<table>
    <tr>
        <th>n</th>
        <th>Container</th>
        <th>Output</th>
    </tr>
    <tr>
        <td><pre><code>1</code></pre></td>
        <td><pre><code></code></pre></td>
        <td><pre><code>Throws "Vec is empty."</code></pre></td>
    </tr>
    <tr>
        <td><pre><code>0</code></pre></td>
        <td><pre><code>1 2</code></pre></td>
        <td><pre><code>1 2</code></pre></td>
    </tr>
    <tr>
        <td><pre><code>1</code></pre></td>
        <td><pre><code>0 0</code></pre></td>
        <td><pre><code>0 0 0</code></pre></td>
    </tr>
    <tr>
        <td><pre><code>2</code></pre></td>
        <td><pre><code>0 5 1</code></pre></td>
        <td><pre><code>0 5 1 5 5</code></pre></td>
    </tr>
    <tr>
        <td><pre><code>3</code></pre></td>
        <td><pre><code>9 1 1 2 3</code></pre></td>
        <td><pre><code>9 1 1 2 3 9 9 9</code></pre></td>
    </tr>
</table>

```cpp
#include <algorithm>
#include <iostream>
#include <vector>

int main() {
    // Read in n.
    int n{0};
    std::cin >> n;

    // Read in the vector.
    int i{0};
    std::vector<int> vec;
    while (std::cin >> i) {
        vec.push_back(i);
    }

    // WRITE YOUR SOLUTION HERE.

    // To print the vector.
    for (auto const& i : vec) {
        std::cout << i << " ";
    }
    std::cout << std::endl;
}
```

<details><summary>Answer</summary><p>

The following solution uses `std::back_inserter`:
```cpp
auto max_iter = std::max_element(vec.begin(), vec.end());

if (max_iter == vec.end()) {
    throw std::out_of_range("Vec is empty.");
}

std::fill_n(std::back_inserter(vec), n, *max_iter);
```

> `std::back_inserter` requires `#include <iterator>`.

The following solution uses `resize()` and some iterator arithmetic:
```cpp
if (vec.empty()) {
    throw std::out_of_range("Vec is empty.");
}

auto max_iter = std::max_element(vec.begin(), vec.end());
vec.resize(vec.size() + n);
std::fill_n(vec.end() - n, n, *max_iter);
```

---

</p></details>

---

## Exercise: Last Occurrence

Write a program which uses an STL algorithm to find the last occurrence of a value given by `std::cin`. The index of the last occurrence should be printed out.

<table>
    <tr>
        <th>Value</th>
        <th>Container</th>
        <th>Output</th>
    </tr>
    <tr>
        <td><pre><code>23</code></pre></td>
        <td><pre><code>1 2 3</code></pre></td>
        <td><pre><code>Throws "Could not find value."</code></pre></td>
    </tr>
    <tr>
        <td><pre><code>1</code></pre></td>
        <td><pre><code>1 1 1 1</code></pre></td>
        <td><pre><code>4</code></pre></td>
    </tr>
    <tr>
        <td><pre><code>2</code></pre></td>
        <td><pre><code>1 1 2 1 1 1</code></pre></td>
        <td><pre><code>2</code></pre></td>
    </tr>
    <tr>
        <td><pre><code>3</code></pre></td>
        <td><pre><code>2 1 2 2 3 2</code></pre></td>
        <td><pre><code>4</code></pre></td>
    </tr>
</table>

```cpp
#include <algorithm>
#include <iostream>
#include <vector>

int main() {
    // Read in the value.
    int val{0};
    std::cin >> val;

    // Read in the vector.
    int i{0};
    std::vector<int> vec;
    while (std::cin >> i) {
        vec.push_back(i);
    }

    // WRITE YOUR SOLUTION HERE.

    std::cout << 42 << std::endl;
}
```

<details><summary>Answer</summary><p>

A solution using the iterator adaptor, `std::make_reverse_iterator`:
```cpp
#include <algorithm>
#include <iostream>
#include <iterator>
#include <stdexcept>
#include <vector>

int main() {
    // Read in the value.
    int val{0};
    std::cin >> val;

    // Read in the vector.
    int i{0};
    std::vector<int> vec;
    while (std::cin >> i) {
        vec.push_back(i);
    }

    auto first = std::make_reverse_iterator(vec.end());
    auto last = std::make_reverse_iterator(vec.begin());
    auto it = std::find(first, last, val);
    if (it == vec.rend()) {
        throw std::runtime_error("Could not find value.");
    }

    std::cout << vec.size() - std::distance(last, it) << std::endl;
}
```

A solution which uses `rbegin()` and `rend()`:
```cpp
#include <algorithm>
#include <iostream>
#include <stdexcept>
#include <vector>

int main() {
    // Read in the value.
    int val{0};
    std::cin >> val;

    // Read in the vector.
    int i{0};
    std::vector<int> vec;
    while (std::cin >> i) {
        vec.push_back(i);
    }

    auto it = std::find(vec.rbegin(), vec.rend(), val);  // Uses std::reverse_iterator.
    if (it == vec.rend()) {
        throw std::runtime_error("Could not find value.");
    }

    std::cout << vec.size() - std::distance(vec.rbegin(), it) << std::endl;
}
```

---

</p></details>

---

## Challenge: C-Style Iterating

Consider the program from **Exercise: Iterators**. Convert the C++ program to its C-style equivalent of iterating. Assume that `vec` has a fixed size of 4.

Hint: `begin()` and `end()` should not be used.

<details><summary>Answer</summary><p>

The purpose of this challenge is to reinforce that iterators are equivalent to pointers. We should also prefer C++ style iterating over C style because C++ iterators are safer to use and works generically with every STL container and algorithm.

```cpp
int main() {
    std::vector<double> vec{0.2, 1.41, 12.2, 43.3};

    for (double* current_ptr{&vec[0]}; current_ptr != &vec[3] + 1; current_ptr++) {
        std::cout << *current_ptr << " ";
    }
    std::cout << std::endl;
}
```

---

</p></details>

---

## Challenge: Counting Different Characters Between 2 Strings

Write a program which counts the number of different characters between 2 strings.

<table>
    <tr>
        <th>Input</th>
        <th>Output</th>
    </tr>
    <tr>
        <td><pre><code>abcdef
abd</code></pre></td>
        <td><pre><code>3</code></pre></td>
    </tr>
    <tr>
        <td><pre><code>123
abc</code></pre></td>
        <td><pre><code>6</code></pre></td>
    </tr>
    <tr>
        <td><pre><code>1234
</code></pre></td>
        <td><pre><code>4</code></pre></td>
    </tr>
</table>

```cpp
#include <iostream>
#include <string>
#include <vector>

int main() {
    std::string a, b;
    std::cin >> a >> b;
    int diff = 0;

    // WRITE YOUR SOLUTION HERE.

    std::cout << diff << std::endl;
}
```

<details><summary>Answer</summary><p>

```cpp
#include <algorithm>
#include <iostream>
#include <map>
#include <numeric>
#include <string>
#include <utility>

int main() {
    std::string a, b;
    std::cin >> a >> b;
    int diff = 0;

    std::map<char, std::pair<int, int>> seen;

    std::for_each(a.begin(), a.end(), [&seen](auto const& i) { seen[i].first++; });
    std::for_each(b.begin(), b.end(), [&seen](auto const& i) { seen[i].second++; });

    for (auto const& p : seen) {
        diff += std::abs(p.second.first - p.second.second);
    }

    std::cout << diff << std::endl;
}
```

---

</p></details>

---

## Challenge: Rock Collector

Given an array of rock sizes.

THe size of the rock is also the number of times the rock can be skipped.

There are `n` rocks along a linear path.

Gary walks along the path. If Gary encounters a rock, he will pick it up and examine it. If the size of the rock is *perfect*, then Gary will add it to his collection.

Gary can only carry `k` rocks in his collection. If Gary is at his carrying capacity i.e. has `k` rocks and wants to add a rock to his collection - then he will discard the most *imperfect* rock.

The function to determine whether a rock is *perfect* has been implemented for you.
- The function will return 0 for rocks that are *perfect*.
- The function will return positive integers for rocks that are *imperfect*.
- The larger the return value, the more *imperfect* the rock.

---

## Feedback

If you liked or disliked anything about the labs, please leave some [feedback](https://forms.office.com/r/sV4X0xR7dT)!
