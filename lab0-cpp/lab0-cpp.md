# Lab0 (C++)

The following table shows what toolchain is prescribed by the course for your operating system. You may choose to set up your own text editor and development environment, however we will not be able to assist you. It is important that you install the minimum compiler version specified for your OS.
<table>
    <tr>
        <th>OS (link to setup guide)</th>
        <th>Text Editor</th>
        <th>Development Environment</th>
        <th>Minimum Compiler Version</th>
    </tr>
    <tr>
        <td><a href="https://code.visualstudio.com/docs/cpp/config-mingw">Windows</a></td>
        <td>VSCode</td>
        <td><a href="https://www.msys2.org/">MinGW via MSYS2</a></td>
        <td><code>g++-5.1</code></td>
    </tr>
    <tr>
        <td><a href="https://code.visualstudio.com/docs/cpp/config-clang-mac">MacOS</a></td>
        <td>VSCode</td>
        <td><a href="https://apps.apple.com/au/app/xcode/id497799835?mt=12">Xcode</a> or <a href="https://stackoverflow.com/questions/9329243/how-to-install-xcode-command-line-tools">Xcode CLI</a></td>
        <td><code>clang++-3.5</code></td>
    </tr>
    <tr>
        <td><a href="https://code.visualstudio.com/docs/cpp/config-linux">Linux</a></td>
        <td>VSCode</td>
        <td>-</td>
        <td><code>g++-5.1</code></td>
    </tr>
</table>

> It is okay if you have a compiler version greater than the number listed in the above table. You just need a [compiler version](https://en.cppreference.com/w/cpp/compiler_support) that at least supports C++14.

> **MacOS**: Although it is very unlikely that programs compiled with `clang++` will behave differently from `g++`, we want you to verify that your solution compiles and behaves as expected using at least `g++-5.1` either by installing `g++` or compiling online with [godbolt](https://godbolt.org/).

## Windows (MinGW)

1. Download VSCode.

    ![win-download-vscode.gif](./win-download-vscode.gif)

1. After downloading VSCode, click through the installer wizard. The "Open with Code" actions are optional.

    ![win-install-vscode.gif](./win-install-vscode.gif)

1. Download MSYS2.

    ![win-download-msys2.gif](./win-download-msys2.gif)

1. After downloading MSYS2, click through the installer wizard.

    ![win-install-msys2.gif](./win-install-msys2.gif)

1. Install MinGW via MSYS2 by running `pacman -S --needed base-devel mingw-w64-x86_64-toolchain` in the MSYS2 terminal. When prompted with "Enter a Selection (default=all)", just press `enter`. Proceed with installation.

    > Note that copying-and-pasting the `pacman` command may not work due to some special characters. Type out the command instead.

    ![win-install-mingw](./win-install-mingw.gif)

1. After installing MinGW, check the minimum version of `gcc` is installed by typing `gcc --version`.

    ![win-check-gcc.gif](./win-check-gcc.gif)

1. Type "Edit environment variables for your account" in the Windows search box to open the "Environment Variables" window. Export MinGW to your environment path by clicking "New" then entering `Path` as the "Variable name" and `C:\msys64\mingw64\bin` as the "Variable value".

    > Note that if you already have an existing `Path`, then "Edit" the `Path` instead.

    ![win-export-mingw-path](./win-export-mingw-path.gif)

## MacOS

1. Download VSCode.

    ![mac-download-vscode.gif](./mac-download-vscode.gif)

1. After installing VSCode, export VSCode to your environment path by pressing `cmd + shift + p` to open the command palette and finding `Shell Command: Install 'code' command in PATH`.

    ![mac-export-code-path.gif](./mac-export-code-path.gif)

1. Install Xcode GUI in the app store or install Xcode CLI by opening the terminal and entering `xcode-select --install`.

    ![mac-install-xcode-cli.gif](./mac-install-xcode-cli.gif)

    It is suggested to install Xcode CLI because Xcode GUI has a lot of bloat and is likely to have this [issue](https://stackoverflow.com/questions/67900692/latest-version-of-xcode-stuck-on-installation-12-5).

1. After installing Xcode, check the minimum version of `clang` is installed by typing `clang --version`.

    ![mac-check-clang.gif](./mac-check-clang.gif)
