# Lab0 (Webots)

Please install [Webots 2021b](https://github.com/cyberbotics/webots/releases/tag/R2021b).

---

## Webots

Webots is a multi-platform robot simulator. It also comes with a basic text editor and build system.

---

## Webots GUI

The Webots GUI contains subwindows:
- <span style="color:red">Simulation View (red)</span>: Contains a view of the world as well as the scene tree.
- <span style="color:blue">Scene Tree (blue)</span>: Contains a hierarchy tree of the world's nodes.
- <span style="color:yellow">Field Editor (yellow)</span>: Selecting a node from the scene tree shows the node's fields.
- <span style="color:green">Text Editor (green)</span>: Basic text editor with build and clean tools.
- <span style="color:pink">Console (pink)</span>: Output from the controller and any world updates.

<img src="webots-window-annotated.png" alt="webots-window-annotated" width="1000px">

---

## Webots Project Structure

A Webots project has a folder structure displayed below:
```
webots-project/
|
|_ controllers/
|  |_ simple_controller/
|
|_ protos/
|  | basic_robot.proto
|
|_ worlds/
   |_ obstacles.wbt
```

The project consists of:
- A `controllers` folder which contains subfolders of controllers
- A `protos` folder which contains descriptions of nodes that can be included in worlds.
- A `worlds` folder which contains `wbt` files.

---

## Worlds

`wbt` files are text files that contain a description of world components e.g.:
- Objects.
- Lighting.
- Floor.
- Background.
- Robots.

The description of the `wbt` is outlined in the [Scene Tree](#scene-tree).

---

## Scene Tree

> Highlighted in the [Webots GUI](#webots-gui) as blue.

A scene tree is a collection of nodes which describes the Webots world.

### Nodes & Fields

Nodes are building blocks of the Webots world and robots. A list of nodes can be found here: https://cyberbotics.com/doc/reference/node-chart

Nodes can have properties such as `translation`, `rotation`, and `name`. These properties are called "fields".

<img src="webots-node-fields.png" alt="webots-node-fields" width="400px">

Fields can be edited within the Webots GUI when the field is selected in the scene tree via the field editor.

> The field editor is highlighted in the [Webots GUI](#webots-gui) as yellow.

---

## Controllers

A controller is a software program (executable/binary) that is used to *control* robots.

Controllers can be created in Webots via `Wizards > New Robot Controller`, then clicking through the Wizard.

<img src="webots-creating-controller.gif" alt="webots-creating-controller" width="1200px">

The created controller can then be attached to an existing robot via its controller node.

<img src="webots-attaching-controller.gif" alt="webots-attaching-controller" width="1200px">

> Note that the controller needs to be compiled before it can be run.

---

## Running the Simulation

To run the Webots simulation, press the play button in the top bar of the simulation view. The simulation is running if the time stamp is changing.

<img src="webots-simulation-view-running.png" alt="webots-simulation-view-running" width="1000">

The Webots simulation can also be fast-forwarded, reversed, or reset.