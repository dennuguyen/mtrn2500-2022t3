#pragma once

#include <iostream>

namespace mtrn2500 {

template <typename U, typename V>
struct Pair {
    Pair() : Pair({}, {}) {}

    Pair(U first, V second) : first(first), second(second) {}

    Pair(Pair const& other) : first(other.first), second(other.second) {}

    Pair(Pair&& other)
        : first(std::exchange(other.first, {})), second(std::exchange(other.second, {})) {}

    Pair& operator=(Pair const& other) {
        first = other.first;
        second = other.second;
        return *this;
    }

    Pair& operator=(Pair&& other) {
        first = std::exchange(other.first, {});
        second = std::exchange(other.second, {});
        return *this;
    }

    ~Pair() = default;

    template <typename X, typename Y>
    operator Pair<X, Y>() {
        return Pair<X, Y>(first, second);
    }

    friend std::ostream& operator<<(std::ostream& os, Pair const& p) {
        os << "(" << p.first << ", " << p.second << ")";
        return os;
    }

    U first;
    V second;
};

template <typename X, typename Y>
Pair<X, Y> make_pair(X x, Y y) {
    return Pair<X, Y>(x, y);
}

};  // namespace mtrn2500