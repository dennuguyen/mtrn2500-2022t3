#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "Complex.solution.hpp"
#include "doctest.h"

TEST_CASE("Default constructor") {
    mtrn2500::Complex<double> actual;
    CHECK(actual.argument() == doctest::Approx(0.0));
    CHECK(actual.modulus() == doctest::Approx(0.0));
}

TEST_CASE("Real constructor") {
    mtrn2500::Complex<int> actual(2);
    CHECK(actual.argument() == 0);
    CHECK(actual.modulus() == 2);
}

TEST_CASE("Real and imaginary constructor") {
    mtrn2500::Complex<int> actual(3, 4);
    CHECK(actual.argument() == 0);
    CHECK(actual.modulus() == 5);
}

TEST_CASE("Conjugate") {
    SUBCASE("Double") {
        mtrn2500::Complex<double> actual(3.3, 4.4);
        mtrn2500::Complex<double> expected(3.3, -4.4);
        CHECK(actual.conjugate() == expected);
    }

    SUBCASE("Integer") {
        mtrn2500::Complex<int> actual(3, 4);
        mtrn2500::Complex<int> expected(3, -4);
        CHECK(actual.conjugate() == expected);
    }
}
