#include <iterator>
#include <vector>

#define DOCTEST_CONFIG_REQUIRE_STRINGIFICATION_FOR_ALL_USED_TYPES
#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"
#include "transform_if.hpp"

namespace std {
template <typename T>
auto operator<<(std::ostream& os, std::vector<T> const& cont) -> std::ostream& {
    os << "[";
    for (auto const& i : cont) {
        os << i << " ";
    }
    os << "]";
    return os;
}

}  // namespace std

TEST_CASE("always_doubles") {
    std::vector<int> nums = {0, 1, 2, 3, 4, 5, 6};
    std::vector<int> expected = {0, 2, 4, 6, 8, 10, 12};
    std::vector<int> actual(nums.size());
    mtrn2500::transform_if(
        nums.begin(), nums.end(), actual.begin(), [](auto const& i) { return true; },
        [](auto const& i) { return i * 2; });
    CHECK(actual == expected);
}

TEST_CASE("never_doubles") {
    std::vector<int> nums = {0, 1, 2, 3, 4, 5, 6};
    std::vector<int> expected = {0, 0, 0, 0, 0, 0, 0};
    std::vector<int> actual(nums.size());
    mtrn2500::transform_if(
        nums.begin(), nums.end(), actual.begin(), [](auto const& i) { return false; },
        [](auto const& i) { return i * 2; });
    CHECK(actual == expected);
}

TEST_CASE("doubles_if_odd") {
    std::vector<int> nums = {0, 1, 2, 3, 4, 5, 6};
    std::vector<int> expected = {2, 6, 10};
    std::vector<int> actual;
    mtrn2500::transform_if(
        nums.begin(), nums.end(), std::back_inserter(actual), [](auto const& i) { return i % 2; },
        [](auto const& i) { return i * 2; });
    CHECK(actual == expected);
}
