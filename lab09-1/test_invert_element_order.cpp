#define DOCTEST_CONFIG_REQUIRE_STRINGIFICATION_FOR_ALL_USED_TYPES
#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"
#include "invert_element_order.hpp"

namespace std {
template <typename T>
auto operator<<(std::ostream& os, std::vector<T> const& cont) -> std::ostream& {
    os << "[";
    for (auto const& i : cont) {
        os << i << " ";
    }
    os << "]";
    return os;
}

TEST_CASE("numerical1") {
    auto v = std::vector<int>({1, 4, 3, 2});
    mtrn2500::invert_element_order(v.begin(), v.end());
    auto w = std::vector<int>({4, 1, 2, 3});
    CHECK(w == v);
}

TEST_CASE("numerical2") {
    auto v = std::vector<int>({5, 2, 3, 1, 5, 4});
    mtrn2500::invert_element_order(v.begin(), v.end());
    auto w = std::vector<int>({1, 4, 3, 5, 1, 2});
    CHECK(w == v);
}

TEST_CASE("numerical3") {
    auto v = std::vector<int>({1, 1, 1, 1, 2});
    mtrn2500::invert_element_order(v.begin(), v.end());
    auto w = std::vector<int>({2, 2, 2, 2, 1});
    CHECK(w == v);
}

TEST_CASE("same") {
    auto v = std::vector<int>({1, 1, 1, 1});
    mtrn2500::invert_element_order(v.begin(), v.end());
    auto w = std::vector<int>({1, 1, 1, 1});
    CHECK(w == v);
}

TEST_CASE("one_out") {
    auto v = std::vector<int>({1, 1, 1, 2});
    mtrn2500::invert_element_order(v.begin(), v.end());
    auto w = std::vector<int>({2, 2, 2, 1});
    CHECK(w == v);
}

TEST_CASE("alpha") {
    auto v = std::vector<std::string>({"abc", "cba", "bde"});
    mtrn2500::invert_element_order(v.begin(), v.end());
    auto w = std::vector<std::string>({"cba", "abc", "bde"});
    CHECK(w == v);
}
