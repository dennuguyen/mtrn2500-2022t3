#pragma once

namespace mtrn2500 {

template <typename InputIt, typename OutputIt, typename UnaryPredicate, typename UnaryOperation>
OutputIt transform_if(InputIt first1, InputIt last1, OutputIt d_first, UnaryPredicate unary_pred,
                      UnaryOperation unary_op) {
    while (first1 != last1) {
        if (unary_pred(*first1)) {
            *d_first++ = unary_op(*first1);
        }
        first1++;
    }
    return d_first;
}

}  // namespace mtrn2500