# MTRN2500 - Lab09-1 (Templates)

## Learning Outcomes

- Understand the concept of generic programming and how C++ achieves this.
- Can write function templates.
- Can write class templates.
- Know that the STL library uses templates.
- Can write a function template in the same style as STL algorithms.

---

## Concept of Templates

What is generic programming? How does C++ achieve this programming paradigm?



What is an example of templates that has already been covered in this course?



How are templates compiled?



---

## Exercise: Function Templates

Convert the following program such that it uses a function template instead of function overloads:

```cpp
#include <iostream>

void print(double var) { std::cout << var << std::endl; }
void print(int var) { std::cout << var << std::endl; }
void print(std::string var) { std::cout << var << std::endl; }
void print(char var) { std::cout << var << std::endl; }

int main() {
    print(3.23);
    print(3);
    print("t3af4");
    print('!');
}
```



---

## Exercise: Class Templates

Recall [Exercise: Complex Numbers]() from `lab02-1`. Modify `Complex` such that it is templated.

> `test_complex.cpp` has been provided.



---

## Exercise: Pair

`std::pair` is limited in functionality (e.g. there is no `operator<<` and no pair conversions). We want to implement our own `Pair` template class according to the following interface.

> `test_pair.cpp` has been provided.

<table>
    <tr>
        <th>Method</th>
        <th>Description</th>
        <th>Usage</th>
        <th>Exceptions</th>
    </tr>
    <tr>
        <td><code>Pair()</code></td>
        <td>Default constructor which default initialises <code>first</code> and <code>second</code>.</td>
        <td><pre><code>Pair&lt;int, std::string&gt; p;</code></pre></td>
        <td>None.</td>
    </tr>
    <tr>
        <td><code>Pair(U first, V second)</code></td>
        <td>Default constructor which initialises <code>first</code> and <code>second</code> with the provided arguments.</td>
        <td><pre><code>Pair&lt;int, std::string&gt; p(42, "42");</code></pre></td>
        <td>None.</td>
    </tr>
    <tr>
        <td><code>Pair(Pair const& other)</code></td>
        <td>Copy constructor.</td>
        <td><pre><code>Pair&lt;int, std::string&gt; p1(42, "42");
Pair&lt;int, std::string&gt; p2(p1);</code></pre></td>
        <td>None.</td>
    </tr>
    <tr>
        <td><code>Pair(Pair&& other)</code></td>
        <td>Move constructor.</td>
        <td><pre><code>Pair&lt;int, std::string&gt; p1(42, "42");
Pair&lt;int, std::string&gt; p2(std::move(p1));</code></pre></td>
        <td>None.</td>
    </tr>
    <tr>
        <td><code>Pair& operator=(Pair const& other)</code></td>
        <td>Copy assignment.</td>
        <td><pre><code>Pair&lt;int, std::string&gt; p1(42, "42");
Pair&lt;int, std::string&gt; p2;
p2 = p1;</code></pre></td>
        <td>None.</td>
    </tr>
    <tr>
        <td><code>Pair& operator=(Pair&& other)</code></td>
        <td>Move assignment.</td>
        <td><pre><code>Pair&lt;int, std::string&gt; p1(42, "42");
Pair&lt;int, std::string&gt; p2;
p2 = std::move(p1);</code></pre></td>
        <td>None.</td>
    </tr>
    <tr>
        <td><code>operator Pair&lt;X, Y&gt;()</code></td>
        <td>Implicitly converts <code>first</code> and <code>second</code> from <code>&lt;U, V&gt;</code> to <code>&lt;X, Y&gt;</code>. This function returns an object of type <code>Pair&lt;X, Y&gt;</code>.</td>
        <td><pre><code>Pair&lt;int, int&gt; p1(42, 32);
Pair&lt;float, float&gt; p2(p1);</code></pre></td>
        <td>None.</td>
    </tr>
    <tr>
        <td><code>friend std::ostream& operator&lt;&lt;(std::ostream& os, Pair const& p)</code></td>
        <td>Writes <code>first</code> and <code>second</code> to output stream with the format <code>(first, second)</code>.</td>
        <td><pre><code>Pair&lt;int, std::string&gt; p(42, "42");
std::cout &lt;&lt; p;</code></pre></td>
        <td>None.</td>
    </tr>
    <tr>
        <td><code>Pair&lt;X, Y&gt; make_pair(X x, Y y)</code></td>
        <td>Free function which constructs a <code>Pair&lt;X, Y&gt;</code> of <code>x</code> and <code>y</code>.</td>
        <td><pre><code>auto p{make_pair(42, "42")};</code></pre></td>
        <td>None.</td>
    </tr>
</table>



---

## Revision: STL Algorithm Predicates & Operations

A lot of STL algorithms will have a function overload for accepting a **predicate**, **operation**, or **function** function:
- Predicates are true/false expressions.
- Operations modify the given inputs.

These functions can have one or two inputs:
- Single inputs work on one element at a time within a range.
- Two inputs work on two consecutive elements at a time within a range.

<table>
    <tr>
        <th>Return Type</th>
        <th>Single Input</th>
        <th>Two Inputs</th>
    </tr>
    <tr>
        <td>Boolean</td>
        <td>UnaryPredicate</td>
        <td>BinaryPredicate</td>
    </tr>
    <tr>
        <td>Element</td>
        <td>UnaryOperation</td>
        <td>BinaryOperation</td>
    </tr>
    <tr>
        <td>Void</td>
        <td>UnaryFunction</td>
        <td>BinaryFunction</td>
    </tr>
</table>

For example, look at the signature and implementation of the following STL algorithms:
- [`std::for_each`](https://en.cppreference.com/w/cpp/algorithm/for_each) has a `UnaryFunction` overload.
- [`std::transform`](https://en.cppreference.com/w/cpp/algorithm/transform) has a `UnaryOperation` and `BinaryOperation` overload.
- [`std::find_if`](https://en.cppreference.com/w/cpp/algorithm/find) has a `UnaryPredicate` overload.


---

## Exercise: `transform_if`

Write a templated function, `transform_if`, which applies the given function, `unary_op`, to the range of iterators `[first, last)` and stores the result of `unary_op` at the output range starting at `d_first`.

The function `unary_op` conditionally executes on the current iterator based on the given predicate function, `unary_pred`, if it is `true`; otherwise, the current iterator is skipped if `unary_pred` is `false`.

`transform_if` returns the end iterator of the output range.

The function declaration is as follows:
```cpp
OutputIt transform_if(InputIt first, InputIt last, OutputIt d_first, UnaryPred unary_pred, UnaryOp unary_op);
```

> `std::transform` and its possible implementation can be referred to here: https://en.cppreference.com/w/cpp/algorithm/transform.

> `test_transform_if.cpp` has been provided. Refer to the tests to see how `transform_if` is used.



---

## Challenge: Invert Element Order

Write a function which inverts the element ordering of a container (in-place) based on the existence of elements in the container and no matter where the element appears in the container. That is, the smallest element is swapped with the biggest element, the second-smallest element is swapped with the second-biggest element, and so on.

<table>
    <tr>
        <th>Input</th>
        <th>Output</th>
    </tr>
    <tr>
        <td>1, 2, 4, 3</td>
        <td>4, 3, 1, 2</td>
    </tr>
    <tr>
        <td>1, 1, 1, 0</td>
        <td>0, 0, 0, 1</td>
    </tr>
    <tr>
        <td>a, c, bac</td>
        <td>c, a, bac</td>
    </tr>
    <tr>
        <td>2, 2, 2</td>
        <td>2, 2, 2</td>
    </tr>
</table>

```cpp
template <typename Iter, typename T = typename std::iterator_traits<Iter>::value_type>
auto invert_element_order(Iter first, Iter last) -> void {
    // WRITE YOUR SOLUTION HERE.
}
```

> `T` is given a default type i.e. `std::iterator_traits<Iter>::value_type` which is the underlying datatype pointed to by `Iter`. The concept of type traits is not covered in this course.

> `test_invert_element_order.cpp` has been provided.



---

## Challenge: Templated `SmallMatrix`

Modify `SmallMatrix` such that it is templated.

---

## Feedback

If you liked or disliked anything about the labs, please leave some [feedback](https://forms.office.com/r/sV4X0xR7dT)!
