#include <sstream>

#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "Pair.hpp"
#include "doctest.h"

TEST_CASE("Default constructor") {
    SUBCASE("int, char") {
        mtrn2500::Pair<int, char> p;
        CHECK(p.first == 0);
        CHECK(p.second == '\0');
    }

    SUBCASE("str, float") {
        mtrn2500::Pair<std::string, float> p;
        CHECK(p.first == "");
        CHECK(p.second == doctest::Approx(0.0));
    }
}

TEST_CASE("Value constructor") {
    SUBCASE("int, str") {
        mtrn2500::Pair<int, std::string> p(42, "42");
        CHECK(p.first == 42);
        CHECK(p.second == "42");
    }

    SUBCASE("float, vec") {
        mtrn2500::Pair<float, std::vector<int>> p(42.42, {1, 2, 3});
        CHECK(p.first == doctest::Approx(42.42));
        CHECK(p.second == std::vector<int>({1, 2, 3}));
    }
}

TEST_CASE("Copy constructor") {
    mtrn2500::Pair<int, std::string> p1(42, "42");
    mtrn2500::Pair<int, std::string> p2(p1);
    CHECK(p2.first == 42);
    CHECK(p2.second == "42");
}

TEST_CASE("Move constructor") {
    mtrn2500::Pair<int, std::string> p1(42, "42");
    mtrn2500::Pair<int, std::string> p2(std::move(p1));
    CHECK(p2.first == 42);
    CHECK(p2.second == "42");
}

TEST_CASE("Copy assignment") {
    mtrn2500::Pair<int, std::string> p1(42, "42");
    mtrn2500::Pair<int, std::string> p2;
    p2 = p1;
    CHECK(p2.first == 42);
    CHECK(p2.second == "42");
}

TEST_CASE("Move assignment") {
    mtrn2500::Pair<int, std::string> p1(42, "42");
    mtrn2500::Pair<int, std::string> p2;
    p2 = std::move(p1);
    CHECK(p2.first == 42);
    CHECK(p2.second == "42");
}

TEST_CASE("Pair conversion") {
    SUBCASE("int -> float") {
        mtrn2500::Pair<int, int> p1(42, 32);
        mtrn2500::Pair<float, float> p2(p1);
        CHECK(p2.first == doctest::Approx(42));
        CHECK(p2.second == doctest::Approx(32));
    }

    SUBCASE("bool -> int") {
        mtrn2500::Pair<bool, bool> p1(false, true);
        mtrn2500::Pair<int, int> p2(p1);
        CHECK(p2.first == 0);
        CHECK(p2.second == 1);
    }
}

TEST_CASE("Output stream") {
    mtrn2500::Pair<int, std::string> p(42, "42");
    std::stringstream ss;
    ss << p;
    CHECK(ss.str() == "(42, 42)");
}

TEST_CASE("Make pair") {
    auto p{mtrn2500::Pair::make_pair(42, "42")};
    CHECK(p.first == 42);
    CHECK(p.second == "42");
}
