#pragma once

#include <cmath>
#include <utility>

namespace mtrn2500 {

template <typename T>
class Complex {
public:
    Complex() = default;
    Complex(T const& real) : Complex(real, 0) {}
    Complex(T const& real, T const& imaginary) : mReal(real), mImaginary(imaginary) {}
    ~Complex() = default;

    Complex conjugate() const { return Complex(mReal, -mImaginary); }

    T modulus() const { return std::sqrt(std::pow(mReal, 2) + std::pow(mImaginary, 2)); }

    T argument() const { return std::atan2(mImaginary, mReal); }

    friend bool operator==(Complex const& lhs, Complex const& rhs) {
        return lhs.mReal == rhs.mReal && lhs.mImaginary == rhs.mImaginary;
    }

private:
    T mReal;
    T mImaginary;
};

}  // namespace mtrn2500