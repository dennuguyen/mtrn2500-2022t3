#pragma once

#include <cmath>
#include <utility>

namespace mtrn2500 {

// MODIFY THIS CLASS.
class Complex {
public:
    Complex() = default;
    Complex(double const& real) : Complex(real, 0) {}
    Complex(double const& real, double const& imaginary) : mReal(real), mImaginary(imaginary) {}
    ~Complex() = default;

    Complex conjugate() const { return Complex(mReal, -mImaginary); }

    double modulus() const { return std::sqrt(std::pow(mReal, 2) + std::pow(mImaginary, 2)); }

    double argument() const { return std::atan2(mImaginary, mReal); }

private:
    double mReal;
    double mImaginary;
};

}  // namespace mtrn2500