# MTRN2500 - Lab03-1 (Memory)

## Learning Outcomes

- Storage duration.
- Dynamic memory allocation.
- Dangers of raw pointers.
- Smart pointers (unique and shared).
- `make_unique` and `make_shared`
- Passing-by-pointer
- References.
- Passing-by-reference.

---

## Exercise: Array Memory Layout

Show that the address of `std::array` is the same as the address of its first element.



Show that the elements of `std::array` are contiguous.



How are the elements of `std::array` allocated in memory?



---

## Exercise: Vector Memory Layout

What is the difference in use-cases between `std::array` and `std::vector`?



Show that the address of `std::vector` is not the same as the address of its first element.



Show that the elements of `std::vector` are contiguous.



How are the elements of `std::vector` allocated in memory?



How can one find more information about what methods `std::vector` has?



---

## Storage Duration

There are three different types of storage duration in C++:
- *Static*: Object persists throughout program life-time.
- *Automatic*: Object persists within scope life-time.
- *Dynamic*: Object life-time is defined by us.

Provide examples of *static*, *automatic*, and *dynamic* variables.



---

## Exercise: Dynamic Memory Allocation

Write a program which safely allocates then deallocates memory for a pointer to an `int` using `malloc` and `free` respectively. What are the disadvantages of using C-style dynamic memory allocation?



Convert the program you created to use C++-style dynamic memory allocation i.e. `new` and `delete`.



What are the dangers of using raw pointers? Provide examples.



---

## Exercise: Smart Pointers

How are smart pointers different from raw pointers? What raw pointer disadvantage does smart pointers address?



What is the difference between unique and shared pointers? What raw pointer disadvantage does shared pointers address?



Convert the following program so that raw pointers are replaced with smart pointers (prefer `std::unique_ptr` over `std::shared_ptr` when possible). The following program may or may not be correct.
```cpp
int main() {
    int* ptr1{new int{1}};
    int* ptr2{new int{2}};
    int* ptr3{ptr2};
    int var{5};
    int* ptr4{new int{var}};
    int* ptr5{new int{var}};
    delete ptr1;
    delete ptr2;
    delete ptr3;
    delete ptr4;
    delete ptr5;
}
```



When should smart pointers not be used?



---

## Exercise: Making Smart Pointers

What are some issues of using `new` to initialise a smart pointer?



Convert the program from **Exercise: Smart Pointers** so that:
- raw pointers are replaced with smart pointers.
- `new` is replaced with `std::make_unique` or `std::make_shared`.



---

## Exercise: Passing-By-Pointer

Convert the following program so that it uses smart pointers instead of raw pointers.

```cpp
#include <memory>

void foo(int* ptr) {
    return;
}

int main() {
    int* ptr{(int*)malloc(sizeof(int))};
    *ptr = 42;
    foo(ptr);
    free(ptr);
}
```



---

## Exercise: References

What are the differences between references and pointers?



What is the output of the following program?

```cpp
#include <iostream>

int main() {
    int a{3};
    int& b{a};
    std::cout << a << " " << b << std::endl;
    a = 4;
    std::cout << a << " " << b << std::endl;
}
```



Are there any dangers to using references? Give an example.



---

## Exercise: Passing-By-Reference

What is the output of the following program?

```cpp
#include <iostream>

void foo(int& arg1, int arg2) {
    arg1++;
    arg2++;
}

int main() {
    int a{3};
    int& b{a};
    int c{4};
    std::cout << a << " " << b << " " << c << std::endl;
    foo(a, b);
    std::cout << a << " " << b << " " << c << std::endl;
    foo(c, a);
    std::cout << a << " " << b << " " << c << std::endl;
}
```



---

## Challenge: Implement a Shared Pointer

Implement a shared pointer to an `int` in a class called `SharedPointer`. The specification for `SharedPointer` is given below:

<table>
    <tr>
        <th>Method</th>
        <th>Description</th>
        <th>Usage</th>
        <th>Exceptions</th>
    </tr>
    <tr>
        <td><code>SharedPointer()</code></td>
        <td>A constructor which initialises a <code>nullptr</code>.</td>
        <td><pre><code>SharedPointer sp;</code></pre></td>
        <td>None</td>
    </tr>
    <tr>
        <td><code>SharedPointer(int*)</code></td>
        <td>A constructor which initialises <code>*this</code> with <code>ptr</code>'s resource.</td>
        <td><pre><code>int* ptr{new int{42}};
SharedPointer sp(ptr);</code></pre></td>
        <td>None</td>
    </tr>
    <tr>
        <td><code>~SharedPointer()</code></td>
        <td>If <code>*this</code> is the last <code>SharedPointer</code> to the resource owned by <code>*this</code>, then the resource is destroyed.</td>
        <td><pre><code>int* ptr{new int{42}};
{
    SharedPointer sp1(ptr);
    SharedPointer sp2(ptr);
    SharedPointer sp3(ptr);
}
*ptr; // Invalid access.</code></pre></td>
        <td>None</td>
    </tr>
    <tr>
        <td><code>int* get()</code></td>
        <td>Returns the stored pointer.</td>
        <td><pre></code>
int* ptr1{new int{42}};
SharedPointer sp(ptr);
int* ptr2{sp.get()};</pre></code></td>
        <td>None</td>
    </tr>
    <tr>
        <td><code>long use_count()</code></td>
        <td>Returns the number of <code>SharedPointer</code> managing <code>*this</code>'s resource.</td>
        <td><pre><code>int* ptr{new int{42}};
SharedPointer sp1(ptr);
SharedPointer sp2(ptr);
SharedPointer sp3(ptr);
sp1.use_count() == 3;</code></pre></td>
        <td>None</td>
    </tr>
    <tr>
        <td><code>int& operator*()</code></td>
        <td>Dereferences the stored pointer in <code>*this</code>.</td>
        <td><pre><code>int* ptr{new int{42}};
SharedPointer sp(ptr);
*sp == 42;</code></pre></td>
        <td>None</td>
    </tr>
</table>



---

## Feedback

If you liked or disliked anything about the labs, please leave some [feedback](https://forms.office.com/r/sV4X0xR7dT)!
